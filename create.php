<?php

session_start();
//print_r($_POST);die();
function saveClass($secretAcessKey, $access_key, $webServiceUrl)
{

    /*************************Start DataBase Connection ******************/
    $servername = "localhost";
    $username = "root";
    $password = "sp";
    $dbname = "privatetutor";
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    include 'connection.php';

    global $errormessage;
    global $msg;

    require_once("AuthBase.php");
    date_default_timezone_set('Asia/Kolkata');
    $title = $_REQUEST['name'];
    $parent_id = $_REQUEST['manager_id'];
    $Presenter_ID = $_REQUEST['teacher_id'];
    $Presenter_Name = $_REQUEST['teacher_name'];
    $Presenter_Email = $_REQUEST['email_to'];
    $guest_email = $_REQUEST['email_from'];
    $start_date = $_REQUEST['end_date'];
    $event_subject = $_REQUEST['event_subject'];
    $_SESSION['subject'] = $_REQUEST['event_subject'];
    //$end_date =  filter_user_input($_REQUEST['end_date']); 

    $class_time = date("g:i A", strtotime($_REQUEST['time'])); //print_r($_REQUEST['time']);
    //$start_date=strtotime($_REQUEST['$start_date']);
    // print_r($start_date);die();

    #$time_in_12_hour_format  = date("g:i A", strtotime($_REQUEST['time']));
    //$class_repeat_type = filter_user_input($_REQUEST['class_repeat_type']);
    $total_users = 10;
    $duration = $_REQUEST['duration'];
    $description = $_REQUEST['desc'];
    $avideo = $_REQUEST['audio'];
    $chkRecordForMe = $_REQUEST['recording']; //my changes
    //if(empty($chkRecordForMe)) { $chkRecordForMe="false";}   //my changes
    //$presenter = filter_user_input($_REQUEST['presenter']);
    //$presenter = explode("|",$_REQUEST['presenter'],3);
    //$Presenter_ID = $presenter[0];
    //$Presenter_Email = $presenter[1];
    //$Presenter_Name = $presenter[2];
    // Calculate TimeStamp ////  
    $minsadd = $duration * 60;
    $strtotime = strtotime($class_time);//print_r($strtotime);die();
    $strtodate = strtotime($start_date);//print_r($strtodate);die();
    $startingdatetime = $strtotime + $strtodate; //print_r($startingdatetime);die();
    $endingdatetime = $startingdatetime + $minsadd;//print_r($endingdatetime);die();

    //calling function from authbase.php
    $authBase = new AuthBase($secretAcessKey, $access_key);
    $method = "create";
    $requestParameters["signature"] = $authBase->GenerateSignature($method, $requestParameters);

    //$requestParameters["presenter_email"]=$Presenter_Email; // Add Teacher email here , Create or Add teacher From the wiziq account after login
    #for room based account pass parameters 'presenter_id', 'presenter_name'
    $requestParameters["presenter_id"] = $Presenter_ID;
    $requestParameters["presenter_name"] = $Presenter_Name;

    // API PARAMETERS////
    $requestParameters["start_time"] = $start_date . " " . $class_time;
    $requestParameters["title"] = $title; //Required
    $requestParameters["duration"] = $duration; //optional
    $requestParameters["time_zone"] = "Asia/Kolkata"; //optional
    $requestParameters["attendee_limit"] = $total_users; //optional // Attendee Limit
    $requestParameters["control_category_id"] = ""; //optional
    //$requestParameters["class_repeat_type"]=$class_repeat_type; //optional
    //$requestParameters["class_end_date"]=$end_date; //optional

    $requestParameters["return_url"] = "http://localhost/privatetutor/?page_id=2052&l=checked&label=feedback&t_id=" . $Presenter_ID . "&stu_id=" . $parent_id; //optional

    $requestParameters["status_ping_url"] = "http://localhost/privatetutor/session_status.php"; //optional
    $requestParameters["language_culture_name"] = "en-us";
    $requestParameters["create_recording"] = "";//$chkRecordForMe; // Either Record The class or not
    $httpRequest = new HttpRequest();

    try {
        $XMLReturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=create', http_build_query($requestParameters, '', '&'));
    } catch (Exception $e) {
        $msg = $e->getMessage();
        $errormessage = 1;
    }
    if (!empty($XMLReturn)) {
        try {
            $objDOM = new DOMDocument();
            $objDOM->loadXML($XMLReturn);

        } catch (Exception $e) {
            $msg = $e->getMessage();
            $errormessage = 1;
        }
        $status = $objDOM->getElementsByTagName("rsp")->item(0);
        $attribNode = $status->getAttribute("status");
        # echo  $time_in_12_hour_format;
        if ($attribNode == "ok") {
            $methodTag = $objDOM->getElementsByTagName("method");
            $method = $methodTag->item(0)->nodeValue;// Method =create
            $class_idTag = $objDOM->getElementsByTagName("class_id");
            $class_session_code = $class_idTag->item(0)->nodeValue; //Class ID
            $recording_urlTag = $objDOM->getElementsByTagName("recording_url");
            $recording_url = $recording_urlTag->item(0)->nodeValue; // Recording URL
            $presenter_emailTag = $objDOM->getElementsByTagName("presenter_email");
            $presenter_email = $presenter_emailTag->item(0)->nodeValue;  // PRESENTER EMAIL
            $presenter_urlTag = $objDOM->getElementsByTagName("presenter_url");
            $presenter_url = $presenter_urlTag->item(0)->nodeValue;  //PRESENTER URL

            //$startdate= getmysqldate($start_date);
            //$enddate= getmysqldate($end_date);


            $sql = "INSERT INTO wp_classes (class_session_code , class_title , manager_id , teacher_id , teacher_name , teacher_email , manager_email , description , start_date , time , total_users , duration , is_recurring , recording_url,presenter_url)
		VALUES ('{$class_session_code}', '{$title}' , '{$parent_id}' , '{$Presenter_ID}' , '{$Presenter_Name}' , '{$Presenter_Email}' , '{$guest_email}' , '{$description}' , '{$start_date}' , '{$class_time}', '{$total_users}', '{$duration}', '{$chkRecordForMe}', '{$recording_url}','{$presenter_url}')";

            //echo $sql;

            /*************  Check Time Event  *******************/
            $duration_time = $duration * 60;
            $endTime = strtotime($class_time) + $duration_time;
            $total_time = date('h:i A', $endTime);
            $check = "SELECT * FROM `wp_classes` WHERE time BETWEEN '{$total_time}' AND '{$class_time}' AND teacher_id = '{$Presenter_ID}' AND start_date = '{$start_date}';";
            $result_date = $conn->query($check);
            if ($result_date->num_rows > 0) {
                // output data of each row
                while ($row = $result_date->fetch_assoc()) {
                    echo "Event already create between" . $class_time . " & " . $total_time;
                }
            } else {

                $result = $conn->query($sql);
                $used_credits = "";
                $event_id = $conn->insert_id;

                switch ($duration) {


                    case 30 :
                        if ($event_subject == 'geography') {
                            $used_credits = 0;
                        } else {
                            $used_credits = 25;
                        }
                        break;
                    case 60 :
                        if ($event_subject == 'geography') {
                            $used_credits = 0;
                        } else {
                            $used_credits = 50;
                        }
                        break;
                    case 90 :
                        $used_credits = 75;
                        break;
                    case 120 :
                        $used_credits = 100;
                        break;
                    case 150 :
                        $used_credits = 125;
                        break;
                    case 2:
                        $used_credits = 10;
                        break;
                }

                $sql_insert = "INSERT into wp_credits_uses(event_id,student_id,teacher_id,used_credits) values('{$event_id}','{$parent_id}','{$Presenter_ID}','{$used_credits}')";
                $result = $conn->query($sql_insert);
                $sql_update = "UPDATE `wp_student_credits` SET `credits_balanace` = (`credits_balanace`-'{$used_credits}') WHERE stu_id = '{$parent_id}'";
                $result = $conn->query($sql_update);
                $time_in_24_hour_format = date("H:i", strtotime($class_time));
                if ($result) {

                    echo saveAttendee($secretAcessKey, $access_key, $webServiceUrl, $class_session_code, $parent_id);


                    /************************ EMAIL SEND FUNCTION START ******************************/
                    $user_mail = 'satnam.spineor@gmail.com';
                    $subject = 'New Event Added';
                    $message = 'Here is the  Detail' . "\n";
                    $message .= 'Start Date ' . $start_date . "\n";
                    $message .= 'Class Time : ' . $class_time . "\n";
                    $message .= 'Duratiom: ' . $duration . "\n";
                    $message .= 'Description: ' . $description . "\n";
                    $header = "From:  . $guest_email . \r\n";
                    $header .= "MIME-Version: 1.0\r\n";
                    $header .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                    $header .= "X-Priority: 1\r\n";
                    if (mail($Presenter_Email, $subject, $message, $header)) {

                        echo "Event created Successfully" . $total_time . ' ' . $class_time;

                        // header('location:http://privatetutor.dyalitsolutions.com/?page_id=2052&l=checked');
                    } else {
                        echo "email is not send but record is inserted";
                    }


//Enroll Student


                } else {
                    echo "Error--: " . $sql . "<br>" . $conn->error;
                }
            }

        } else if ($attribNode == "fail") {
            $error = $objDOM->getElementsByTagName("error")->item(0);
            $errorcode = $error->getAttribute("code"); // Define Error code
            $msg = $error->getAttribute("msg");  // Define Error Message
            $errormessage = 1;
        }
    }//end if


    return $msg;
    return $errormsg;
    return $errormessage;
}


function saveAttendee($secretAcessKey, $access_key, $webServiceUrl, $class_session_code, $studentID)
{
    $servername = "localhost";
    $username = "root";
    $password = "sp";
    $dbname = "privatetutor";
    // Create connection
    $con = mysql_connect($servername, $username, $password);
    mysql_select_db($dbname, $con);
    // Check connection
    //Get additional info of child from db
    $qry = mysql_query("select * from register_student where id='$studentID'", $con);
    $fetch = mysql_fetch_array($qry);
    $email = $fetch['Email'];
    $fname = $fetch['First_Name'];
    $lname = $fetch['Last_Name'];
    $pno = $fetch['Phone'];
    $name = $fname . " " . $lname;
    $studentName = $name;
    //Get additional info of child from db
    $class_code = $class_session_code;// Session code
    //Check if student already enrolled
    $query = sprintf("select class_session_code from class_students where class_session_code= %d and student_id = %d", (int)$class_code, (int)$studentID);
    $result = mysql_query($query, $con);
    if (mysql_num_rows($query) <= 0) {

        $authBase = new AuthBase($secretAcessKey, $access_key);
        //Create XML to send to API
        $XMLAttendee = "<attendee_list>
						<attendee>
							<attendee_id><![CDATA[101]]></attendee_id>
							<screen_name><![CDATA[john]]></screen_name>
							<language_culture_name><![CDATA[es-ES]]></language_culture_name>
						</attendee>
					</attendee_list>";

        $XMLAttendee1 = "<attendee_list>
	  					<attendee>
							<attendee_id><![CDATA[";
        $XMLAttendee2 = "]]></attendee_id>

        <screen_name><![CDATA[";

        $XMLAttendee3 = "]]></screen_name>

                     <language_culture_name><![CDATA[en-us]]></language_culture_name>

      				</attendee>

      			</attendee_list>";

        $FinalXMLAttendee = $XMLAttendee1 . $studentID . $XMLAttendee2 . $studentName . $XMLAttendee3;

//Pass API parameters
        $method = "add_attendees";
        $requestParameters["signature"] = $authBase->GenerateSignature($method, $requestParameters);
        $requestParameters["class_id"] = $class_code;//required
        $requestParameters["attendee_list"] = $FinalXMLAttendee;

        $httpRequest = new HttpRequest();

        try {
            $XMLReturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=add_attendees', http_build_query($requestParameters, '', '&'));

        } catch (Exception $e) {
            $e->getMessage();
        }

        if (!empty($XMLReturn)) {
            try {
                $objDOM = new DOMDocument();
                $objDOM->loadXML($XMLReturn);
            } catch (Exception $e) {
                $e->getMessage();
            }

            $status = $objDOM->getElementsByTagName("rsp")->item(0);

            $attribNode = $status->getAttribute("status");

            if ($attribNode == "ok") {
                $methodTag = $objDOM->getElementsByTagName("method");
                $method = $methodTag->item(0)->nodeValue;

                $class_idTag = $objDOM->getElementsByTagName("class_id");
                $class_session_code = $class_idTag->item(0)->nodeValue;

                $add_attendeesTag = $objDOM->getElementsByTagName("add_attendees")->item(0);
                $add_attendeesStatus = $add_attendeesTag->getAttribute("status");

                $attendeeTag = $objDOM->getElementsByTagName("attendee");
                $length = $attendeeTag->length;

                for ($i = 0; $i < $length; $i++) {
                    $attendee_idTag = $objDOM->getElementsByTagName("attendee_id");
                    $attendee_id = $attendee_idTag->item($i)->nodeValue;
                    $attendee_urlTag = $objDOM->getElementsByTagName("attendee_url");
                    $attendee_url = $attendee_urlTag->item($i)->nodeValue;
                }

                ///////// ADD STUDENT TO CLASS IN DATABASE/////////////////////////


                $query123 = mysql_query("select * from wp_classes where class_session_code='$class_session_code'", $con);

                $fetchdate = mysql_fetch_array($query123);

                $date = $fetchdate['start_date'];

                $time = $fetchdate['time'];

                $duration = $fetchdate['duration'];

                $minsadd = $duration * 60;

                $strtotime = strtotime($time);


                $strtodate = strtotime($date);

                // $startingdatetime=$strtotime + $strtodate;
                $startingdatetime = $strtotime;


                // $endingdatetime=$startingdatetime + $minsadd;
                $endingdatetime = $strtotime + $minsadd;
                // echo $startingdatetime.'    '.$endingdatetime;


                $sql = "insert into class_students (class_session_code,student_id,student_name,attendee_id,attendee_url,email,fname,lname,stream,pno,date,time,duration,startingtimestamp,endingtimestamp) values(\"" . $class_session_code . "\",\"" . $studentID . "\",\"" . $name . "\",\"" . $attendee_id . "\",\"" . $attendee_url . "\",\"" . $email . "\",\"" . $fname . "\",\"" . $lname . "\",\"" . $stream . "\",\"" . $pno . "\",\"" . $date . "\",\"" . $time . "\",\"" . $duration . "\",\"" . $startingdatetime . "\",\"" . $endingdatetime . "\")";

                mysql_query($sql, $con) or die(mysql_error());


// we have chgd  class_session_code

                $query = mysql_query("select * from wp_classes where class_session_code='$class_session_code' ", $con);

                $classdetails = mysql_fetch_array($query);

                $class_title = $classdetails['class_title'];

                $class_date = $classdetails['date'];

                $class_time = $classdetails['time'];

                $description = $classdetails['description'];


                $from = 'admin@opendais.xyz';

                $subject = $class_title;

                $message = '<html><body>';

                $message .= "<table cellpadding=4 align=center width=70% bordercolor=#ccc border=1>";

                $message .= "<tr><td align=center colspan=2 bgcolor=#FFFFCC height=50px>

        <strong>ONLINE CLASS SCHEDULE</strong></td></tr>";

                $message .= "<tr><td align=right><strong>Class Title :</strong> </td><td>" . $class_title . "</td></tr>";

                $message .= "<tr><td align=right><strong>Class Date :</strong> </td><td>" . $class_date . "</td></tr>";

                $message .= "<tr><td align=right><strong>Class Time :</strong> </td><td>" . $class_time . "</td></tr>";

                $message .= "<tr><td align=right><strong>About the class :</strong> </td><td>" . $description . "</td></tr>";

                $message .= "<tr><td align=right><strong>Enter the Class  :</strong> </td><td><a href='http://opendais.xyz/liveclasses/index.php' target='_blank'>Click Here</a></td></tr>";

                $message .= "</table><br /><br />";

                $message .= '</body></html>';

                $headers = "From: " . $from . "\r\n";

                $headers .= "Reply-To: " . $from . "\r\n";

                $headers .= "MIME-Version: 1.0\r\n";

                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


                mail($email, $subject, $message, $headers);

                $msg = "Student added to class successfully.";


                ///////// ADD STUDENT TO CLASS IN DATABASE END/////////////////////////


            } else if ($attribNode == "fail") {

                $error = $objDOM->getElementsByTagName("error")->item(0);

                $errorcode = $error->getAttribute("code");

                $msg = $error->getAttribute("msg");

            }

        }//end if

    } //END IF $check

    else {
        $msg = "Student already added to the class. ";
    }


    return $msg;

}//end add attendee function


$secretAcessKey = "Eo2oCm+BYsHyKit28sc6iA==";//secretAcessKey;
$access_key = "kilNGCgBbis=";//access_key;
$webServiceUrl = "http://class.api.wiziq.com/";
echo $msg = saveClass($secretAcessKey, $access_key, $webServiceUrl);
 
