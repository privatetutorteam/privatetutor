<?php
/*
Plugin Name: Student Teacher Management
*/

//menu items
add_action('admin_menu','private_tutor_schools_modifymenu');
function private_tutor_schools_modifymenu() {
	
	//this is the main item for the menu
	add_menu_page('Students', //page title
	'Students', //menu title
	'manage_options', //capabilities
	'private_tutor_students_list', //menu slug
	'private_tutor_students_list' //function
	);
	

//this is the main item for the menu
	add_menu_page('Teachers', //page title
	'Teachers', //menu title
	'manage_options', //capabilities
	'private_tutor_teachers_list', //menu slug
	'private_tutor_teachers_list' //function
	);	

       add_submenu_page(null,#parent slug
	'Edit Student',
	'Edit Student',
	'manage_options',
	'edit_student',
	'edit_student');
	
	add_submenu_page(null,#parent slug
	'Save Student',
	'Save Student',
	'manage_options',
	'save_student',
	'save_student');
	
	
	add_submenu_page(null,#parent slug
	'Edit Teacher',
	'Edit Teacher',
	'manage_options',
	'edit_teacher',
	'edit_teacher');
	
	add_submenu_page(null,#parent slug
	'Save Teacher',
	'Save Teacher',
	'manage_options',
	'save_teacher',
	'save_teacher');
	
	add_submenu_page(null,#parent slug
	'Show Classes',
	'Show Classes',
	'manage_options',
	'show_student_classes',
	'show_student_classes');
	
	add_submenu_page(null,#parent slug
	'Show TClasses',
	'Show TClasses',
	'manage_options',
	'show_teacher_classes',
	'show_teacher_classes');
	
}

add_action('admin_menu','private_tutor_schools_newmenu');
function private_tutor_schools_newmenu(){
add_menu_page('Subjects', //page title
	'Manage Subjects', //menu title
	'manage_options', //capabilities
	'private_tutor_skill_add', //menu slug
	'private_tutor_skill_add' //function
	);

}

add_action('admin_menu','private_tutor_schools_attendance_menu');
function private_tutor_schools_attendance_menu(){
add_menu_page('Attendance', //page title
	'Attendance ', //menu title
	'manage_options', //capabilities
	'student_attendance', //menu slug
	'student_attendance' //function
	);

}

define('ROOTDIR', plugin_dir_path(__FILE__));
require_once('students-list.php');
require_once('students-functions.php');
require_once('teachers-list.php');