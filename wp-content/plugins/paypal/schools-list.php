<?php

function paypal_list () {
?>

<div class="wrap">
<h2>Paypal Settings</h2>

<?php
global $wpdb;
$rows = $wpdb->get_results("SELECT * from wp_paypal");
foreach ($rows as $row ){
	$id = $row->id;
	$title = $row->title;
	$email = $row->email;
	$currency = $row->currency;
	$return_url = $row->return_url;
}
?>


<div class="wrap">
<form method="post" action="<?php echo admin_url('admin.php?page=paypal_settings_update')?>" enctype="multipart/form-data">
<table class='wp-list-table widefat fixed striped pages'>
<input type="hidden" name="id" value="<?php echo $id; ?>">
<tr><th>Title</th><td><input type="text" name="title" value="<?php echo $title;?>" required="Fill This Field Please"/></td></tr>
<tr><th>Paypal Email</th><td><input type="text" name="email" value="<?php echo $email;?>" required="Fill This Field Please"/></td></tr>
<tr><th>Currency</th><td>
<select id="paypal_payment_currency" name="paypal_payment_currency">
    <option value="USD" selected="">US Dollar</option>
    <option value="GBP">Pound Sterling</option>
    <option value="EUR">Euro</option> 
	<option value="AUD">Australian Dollar</option>
    <option value="CAD">Canadian Dollar</option>   
	<option value="NZD">New Zealand Dollar</option>    
	<option value="HKD">Hong Kong Dollar</option>     
</select>
</td></tr>
<tr><th>Return Url</th><td><?php echo site_url(); ?>/?page_id=<input type="text" name="return_url" value="<?php echo $return_url;?>" required=" Please"/> </td>

</tr>
<tr><th></th><td><input type='submit' name="update" value='Save Settings' class='button'> </td>

</tr>
&nbsp;&nbsp;
</form>

</div>
<style type="text/css">
tbody tr td input
{
	width:50%;
	padding:10px;
}
</style>
<?php
}