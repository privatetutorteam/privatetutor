<?php

function paypal_settings_update() {
global $wpdb;
	$id = $_GET["id"];
	$title=$_POST["title"];
	$email=$_POST["email"];
	$currency = $_POST['paypal_payment_currency'];
	$return_url = $_POST['return_url'];		
	$result =	$wpdb->update( 
	'wp_paypal', 
	array( 
		'title' => $title,	// string
		'email' => $email,
		'currency' => $currency,
		'return_url' => $return_url
	), 
	array( 'id' => 1 ), 
	array( 
		'%s',
		'%s',
		'%s',
		'%d'
		// value1
	), 
	array( '%d' ) 
);
if($result)
	{
		?>
				<script type="text/javascript">
				
				 window.location.assign("admin.php?page=paypal_list")
				
			</script>
            <?php
	}
	else
	{
		?>
				<script type="text/javascript">
				alert("Record Is not update something wrong here");
			</script>
            <?php
	}
}
?>