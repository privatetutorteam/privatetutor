<?php
/**
 * Post Grid
 *
 */
if (!function_exists('search_shortcode')) {

	function search_shortcode( $atts, $content = null, $shortcodename = '' ) {
		extract(shortcode_atts(array(
			'custom_class'    => ''
		), $atts));

		$output = '<div class="search-box clearfix">';

		$output .= '<h4>'.__('Tutor Search', CURRENT_THEME).'</h4>';
		$output .= '<form role="search" action="'.get_bloginfo('url').'" method="get" id="searchformcategory">';
	
		$output .= '<fieldset class="fieldset-portfolio_category">';
			$output .= '<label for="cat">'.__('Subject', CURRENT_THEME).'</label>';
			$output .= '<select name="cat" id="cat">';

			$output .= '<option value="none" disabled selected>Please Select</option>';

			$taxonomy = 'portfolio_category';
			$terms = get_terms($taxonomy); // Get all terms of a taxonomy

			if ( $terms && !is_wp_error( $terms ) ) :
			    foreach ( $terms as $term ) {
			    	$output .= '<option value="'.$term->term_taxonomy_id.'">' . $term->name . '</option>';
			    }
			endif;

			$output .= '</select>';
		$output .= '</fieldset>';

		$output .= '<fieldset class="fieldset-portfolio_info">';
			$output .= '<label for="city">'.__('City', CURRENT_THEME).'</label>';

			$output .= '<select name="city" id="city">';

			$output .= '<option value="none" disabled selected>Please Select</option>';

				global $wpdb; 
				$meta_key = 'tz_portfolio_info';
				$allInfo = $wpdb->get_results("SELECT * FROM `wp_postmeta` WHERE `meta_key` LIKE 'tz_portfolio_info' LIMIT 0, 1000");

				for($i=0; $i<count($allInfo); $i++) {
					$allMetas[] = $allInfo[$i]->meta_value;
				}

				$allMetasUniq = array_unique($allMetas);
				sort($allMetasUniq);

				foreach ($allMetasUniq as $key => $value) {
					$output .= '<option value="'.$value.'">' . $value . '</option>';
				}
			$output .= '</select>';
		$output .= '</fieldset>';

		$output .= '<input type="hidden" name="s" value=""/>';

		$output .= '<input type="hidden" name="teacher_search" value="true"/>';
		$output .= '<input type="submit" alt="Search" value="'.__('Search', CURRENT_THEME).'" />';

		$output .= '</form></div>';

		$output = apply_filters( 'cherry_plugin_shortcode_output', $output, $atts, $shortcodename );

		return $output;
	}
	add_shortcode('search_box', 'search_shortcode');
}?>