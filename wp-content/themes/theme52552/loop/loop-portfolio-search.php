<?php /* Loop Name: Search Portfolio */ ?>


<?php
global $query_string;

$termslug = $_GET['cat'];
$searchString = $_GET['city'];

if($termslug == '') {
	$taxonomy = 'portfolio_category';
	$terms = get_terms($taxonomy);

	if ( $terms && !is_wp_error( $terms ) ) :
	    foreach ( $terms as $term ) {
	    	$termslug[] = $term->term_taxonomy_id;
	    }
	endif;
}
$i = 1;

if ( get_query_var('paged') ) {
 $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) {
 $paged = get_query_var('page');
} else {
 $paged = 1;
}

$args = array( 
	'post_type'        => 'portfolio',
	'tax_query' => array(
		array(
			'taxonomy' => 'portfolio_category', 
			'field' => 'term_taxonomy_id', 
			'terms'=> $termslug,
			'operator' => 'IN'
		),
	),
	'showposts'    => 24,
	'paged'        => $paged,
	'meta_key'     => 'tz_portfolio_info', 
	'meta_value'   => $searchString, 
	'meta_compare' => 'like'
);
$the_query = new WP_Query( $args );

if( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
	$posts      	= get_posts($args);
	$excerpt        = get_the_excerpt();
	$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' );
	$url            = $attachment_url['0'];
	$image          = aq_resize($url, 300, 300, true);
	$mediaType      = get_post_meta($post_id, 'tz_portfolio_type', true);

?>
	<article class="item-<?php echo $i; ?>">
		<?php if(has_post_thumbnail($post_id)) {
			echo '<figure class="featured-thumbnail thumbnail">';
			echo '<a href="'.get_permalink($post_id).'" title="'.get_the_title($post_id).'">';
			echo '<img  src="'.$image.'" alt="'.get_the_title($post_id).'" />';
			echo '</a></figure>';
		} ?>
		<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	</article>

<?php $i++; endwhile; else: ?>

<div class="no-results">
	<?php echo '<p><strong>' . __('Nothing Found', CURRENT_THEME) . '</strong></p>'; ?>
	<p><?php echo theme_locals("we_apologize"); ?> <a href="<?php echo home_url(); ?>/" title="<?php bloginfo('description'); ?>"><?php echo theme_locals("return_to"); ?></a>.</p>
</div><!--no-results-->
<?php endif; ?>

<div class="clear"></div>

<?php
	//get_template_part('includes/post-formats/post-nav');
	wp_reset_postdata();
?>