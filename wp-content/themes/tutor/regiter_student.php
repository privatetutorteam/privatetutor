<?php
session_start();
/*
Template Name: Student Registration
*/
?>
<?php 
ob_start();
?>

<?php get_header();?>
<?php
	mysql_connect("localhost","privatetutor","sp");
	mysql_select_db("privatetutor");
	global $wpdb;
	$results = $wpdb->get_results( 'SELECT username, Email FROM register_student', ARRAY_A );
	foreach($results as $result) {
		$result_username[] = $result[username];
		$result_email[] = $result[Email];
	}

?>
<script type="text/javascript">
        
	var username_array = <?php echo json_encode($result_username); ?>;
	var email_array = <?php echo json_encode($result_email); ?>;
    $(document).ready(function(){
    	// Smart Wizard     	
     $('#wizard').smartWizard({transitionEffect:'slideleft',onLeaveStep:leaveAStepCallback});

      function leaveAStepCallback(obj){
        var step_num= obj.attr('rel');
        return validateSteps(step_num);
      }
      
      function onFinishCallback(){
       if(validateAllSteps()){
        $('form').submit();
       }
      }
		});
	   
    function validateAllSteps(){
       var isStepValid = true;
       
       if(validateStep1() == false){
         isStepValid = false;
         $('#wizard').smartWizard('setError',{stepnum:1,iserror:true});         
       }else{
         $('#wizard').smartWizard('setError',{stepnum:1,iserror:false});
       }
       
       if(validateStep3() == false){
         isStepValid = false;
         $('#wizard').smartWizard('setError',{stepnum:3,iserror:true});         
       }else{
         $('#wizard').smartWizard('setError',{stepnum:3,iserror:false});
       }
       
       if(!isStepValid){
          $('#wizard').smartWizard('showMessage','Please correct the errors in the steps and continue');
       }
              
       return isStepValid;
    } 	
		
		
		function validateSteps(step){
		  var isStepValid = true;
      // validate step 1
      if(step == 1){
        if(validateStep1() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      // validate step3
      if(step == 3){
        if(validateStep3() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
        }else{
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
      }
      
      return isStepValid;
    }
		
    function validateStep1(){
       var isValid = true; 
       // Validate Username
       var un = $('#username').val();
       if(!un && un.length <= 0){
         isValid = false;
         $('#msg_username').html('Please fill username').show();
       }else{
         $('#msg_username').html('').hide();
        if(username_array!=null){
	for(i = 0; i < username_array.length; i++){		
		if(un == username_array[i]) {
			isValid = false;
			$('#msg_username').html('username already exists').show();
		}
	}}
       }       	
       // validate password
       var pw = $('#password').val();
       if(!pw && pw.length <= 0){
         isValid = false;
         $('#msg_password').html('Please fill password').show();         
       }else{
         $('#msg_password').html('').hide();
       }
       
       // validate confirm password
       var cpw = $('#cpassword').val();
       if(!cpw && cpw.length <= 0){
         isValid = false;
         $('#msg_cpassword').html('Please fill confirm password').show();         
       }else{
         $('#msg_cpassword').html('').hide();
       }  
       
       // validate password match
       if(pw && pw.length > 0 && cpw && cpw.length > 0){
         if(pw != cpw){
           isValid = false;
           $('#msg_cpassword').html('Password mismatch').show();            
         }else{
           $('#msg_cpassword').html('').hide();
         }
       }

	// validate username unique
	
       return isValid;
    }
    
    function validateStep3(){
      var isValid = true;    
      //validate email  email
      var email = $('#email').val();
       if(email && email.length > 0){
         if(!isValidEmailAddress(email)){
           isValid = false;
           $('#msg_email').html('Email is invalid').show();           
         }else{
          $('#msg_email').html('').hide();	
         } 
       }else{
         isValid = false;
         $('#msg_email').html('Please enter email').show();
       }
        if(email_array!=null){
	for(i = 0; i < email_array.length; i++){
		if(email == email_array[i]) {
			isValid = false;
			$('#msg_email').html('email already exists').show();
		}		
	}}
      return isValid;
    }
    
    // Email Validation
    function isValidEmailAddress(emailAddress) {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      return pattern.test(emailAddress);
    } 
		
		
</script>

<table align="center" border="0" cellpadding="0" cellspacing="0">
<tr><td>


<?php
//	$target_dir = get_template_directory();
//	echo "dir ".$target_dir;
   if(isset($_REQUEST['issubmit'])){
      $username=$_POST['username'];
	   $password=$_POST['password'];
	   $firstname=$_POST['firstname'];
	   $lastname=$_POST['lastname'];
	   $gender=$_POST['gender'];
	   $email=$_POST['email'];
	   $phone=$_POST['phone'];
	   $address=$_POST['address'];
	
	   $about=$_POST['about'];
	   $profilepic=$_POST['profilepic'];
	
	// Image Upload
	
	
	
	$target_dir =  WP_SITEURL."wp-content/themes/tutor/images/uploads";
	$target_file = $target_dir . '/' . $_FILES["profilepic"]["name"];
	
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	
	    $check = getimagesize($_FILES["profilepic"]["tmp_name"]);
	    if($check !== false) {
	        echo "File is an image - " . $check["mime"] . ".";
	        $uploadOk = 1;
	    } else {
	        echo "File is not an image.";
	        $uploadOk = 0;
	    }
	
	// Check if file already exists
	if (file_exists($target_file)) {
	    echo "Sorry, file already exists.";
	    $uploadOk = 0;
	}
	// Check file size
	if ($_FILES["profilepic"]["size"] > 5000000000) {
	    echo "Sorry, your file is too large.";
	    $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["profilepic"]["tmp_name"], $target_file)) {
	        echo "The file ". basename( $_FILES["profilepic"]["name"]). " has been uploaded.";
	    } else {
	        echo '<script>alert("Error");</script>';
	    }
	}

	
	

	$_SESSION['username'] = $username;
	$_SESSION['email'] = $email;
	$image = $_FILES["profilepic"]["name"];
		$query="INSERT INTO register_student (Username, password,First_name,Last_Name,Gender,Email,Phone,Address,About,profilepic)
VALUES ('$username','$password','$firstname','$lastname','$gender','$email','$phone','$address','$about','$image')";
		$result = mysql_query($query);
    		
		if($result)
		{
		   /************************ EMAIL SEND FUNCTION START ******************************/
				$user_mail = 'satnam.spineor@gmail.com';
				$subject = 'Your username and password info';
				#$message = 'You have registered at IMS';
				$message = 'Here is the login Detail' . "\n";
				$message .= 'Website Link ' . get_site_url() . "\n";
				$message .= 'UserName: ' . $username . "\n";
				$message .= 'Password: ' . $password . "\n";
				$header = "From:  satnam.spineor@gmail.com \r\n"; 
				$header.= "MIME-Version: 1.0\r\n"; 
				$header.= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 
				$header.= "X-Priority: 1\r\n"; 
				if(mail ($email,$subject, $message, $headers))
				{
				 $_SESSION["username"] = $username;
			         header("location:?page_id=2052");
			         
			         
			         


				}
		}
		else{
		 echo "<strong>form is sumbitted</strong>";
		}
   }

?>
 
<form action="#" method="POST" id="formCheckPassword" enctype="multipart/form-data">
  <input type='hidden' name="issubmit" value="1">
<!-- Tabs -->
  		<div id="wizard" class="swMain">
  			<ul>
  				<li><a href="#step-1">
                <span class="stepNumber">1</span>
                <span class="stepDesc">
                   Account Details<br />
                   <small>Fill your account details</small>
                </span>
            </a></li>
  				<li><a href="#step-2">
                <span class="stepNumber">2</span>
                <span class="stepDesc">
                   Profile Details<br />
                   <small>Fill your profile details</small>
                </span>
            </a></li>
  				<li><a href="#step-3">
                <span class="stepNumber">3</span>
                <span class="stepDesc">
                   Contact Details<br />
                   <small>Fill your contact details</small>
                </span>
             </a></li>
  				<li><a href="#step-4">
                <span class="stepNumber">3</span>
                <span class="stepDesc">
                   Other Details<br />
                   <small>Fill your other details</small>
                </span>
            </a></li>
  			</ul>
  			<div id="step-1">	
            <h2 class="StepTitle">Step 1: Account Details</h2>
            <table cellspacing="3" cellpadding="3" align="center">
          			<tr>
                    	<td align="center" colspan="3">&nbsp;</td>
          			</tr>        
          			<tr>
                    	<td align="right">Username :</td>
                    	<td align="left">
                    	  <input type="text" id="username" name="username" value="" class="txtBox" " required>
                      </td>
                    	<td align="left"><span id="msg_username"></span>&nbsp;</td>
          			</tr>
          			<tr>
                    	<td align="right">Password :</td>
                    	<td align="left">
                    	  <input type="password" id="password" name="password" value="" class="txtBox"  required>
                      </td>
                    	<td align="left"><span id="msg_password"></span>&nbsp;</td>
          			</tr> 
                <tr>
                    	<td align="right">Confirm Password :</td>
                    	<td align="left">
                    	  <input type="password" id="cpassword" name="cpassword" value="" class="txtBox"  required>
                      </td>
                    	<td align="left"><span id="msg_cpassword"></span>&nbsp;</td>
          			</tr>                                   			
  			   </table>          			
        </div>
  			<div id="step-2">
            <h2 class="StepTitle">Step 2: Profile Details</h2>	
            <table cellspacing="3" cellpadding="3" align="center">
          			<tr>
                    	<td align="center" colspan="3">&nbsp;</td>
          			</tr>        
          			<tr>
                    	<td align="right">First Name :</td>
                    	<td align="left">
                    	  <input type="text" id="firstname" name="firstname" value="" class="txtBox"  required>
                      </td>
                    	<td align="left"><span id="msg_firstname"></span>&nbsp;</td>
          			</tr>
          			<tr>
                    	<td align="right">Last Name :</td>
                    	<td align="left">
                    	  <input type="text" id="lastname" name="lastname" value="" class="txtBox"  required>
                      </td>
                    	<td align="left"><span id="msg_lastname"></span>&nbsp;</td>
          			</tr> 
          			<tr>
                    	<td align="right">Gender :</td>
                    	<td align="left">
                        <select id="gender" name="gender" class="txtBox">
                          <option value="">-select-</option>
                          <option value="Female">Female</option>
                          <option value="Male">Male</option>                 
                        </select>
                      </td>
                    	<td align="left"><span id="msg_gender"></span>&nbsp;</td>
          			</tr> 
				<tr>
                    	<td align="right">Profile Picture</td>
                    	<td align="left">
                    	  <input type="file" id="profilepic" name="profilepic" value="" class="txtBox">
                      </td>
                    	<td align="left"><span id="msg_profilepic"></span>&nbsp;</td>
          			</tr>                                   			
  			   </table>        
        </div>                      
  			<div id="step-3">
            <h2 class="StepTitle">Step 3: Contact Details</h2>	
            <table cellspacing="3" cellpadding="3" align="center">
          			<tr>
                    	<td align="center" colspan="3">&nbsp;</td>
          			</tr>        
          			<tr>
                    	<td align="right">Email :</td>
                    	<td align="left">
                    	  <input type="email" id="email" name="email" value="" class="txtBox">
                      </td>
                    	<td align="left"><span id="msg_email"></span>&nbsp;</td>
          			</tr>
          			<tr>
                    	<td align="right">Phone :</td>
                    	<td align="left">
                    	  <input type="text" id="phone" name="phone" value="" class="txtBox"  required>
                      </td>
                    	<td align="left"><span id="msg_phone"></span>&nbsp;</td>
          			</tr>          			
          			<tr>
                    	<td align="right">Address :</td>
                    	<td align="left">
                            <textarea name="address" id="address" class="txtBox" rows="3"  required></textarea>
                      </td>
                    	<td align="left"><span id="msg_address"></span>&nbsp;</td>
          			</tr>                                   			
  			   </table>               				          
        </div>
  			<div id="step-4">
            <h2 class="StepTitle">Step 4: Other Details</h2>	
            <table cellspacing="3" cellpadding="3" align="center">
          			<tr>
                    	<td align="center" colspan="3">&nbsp;</td>
          			</tr>        
          					
          			<tr>
                    	<td align="right">About You :</td>
                    	<td align="left">
                            <textarea name="about" id="about" class="txtBox" rows="5"  required></textarea>
                      </td>
                    	<td align="left"><span id="about_you"></span>&nbsp;</td>
          			</tr>                                   			
  			   </table>                 			
        </div>
  		</div>
<!-- End SmartWizard Content -->  		
</form> 
  		
</td></tr>
</table> 
<?php
get_footer();
?>
<?php 
ob_flush();
?>