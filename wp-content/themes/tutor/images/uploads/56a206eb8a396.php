<script type="text/javascript">
        var _mfq = _mfq || [];
        (function () {
        var mf = document.createElement("script"); mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/5a340296-d2a8-4c8b-99d5-20bb502649b3.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
      })();
    </script>
<?php
/*
Template Name: REVERSE 
*/
?>
 <!DOCTYPE html>
<html lang="en">
<head>
  <title>Revese Mortgage</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link href='https://fonts.googleapis.com/css?family=Viga' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,700italic,700,900,600italic,600,400italic,300italic,300,200italic,200' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/reversecss/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/reversecss/style.css" />
  <style>
#mf151 p{
font-family: "PT Sans",Arial,Helvetica,sans-serif !important;
font-size: 34px !important;
font-weight: 700;
color: rgb(44, 35, 35);
}
</style>
  <style type="text/css">
.modal-body > ul {
    margin-left: 2%;
}
.modal-content {
    width: 137%;
    
}
/***********button tellmemore popup css*********************/

#popup-qwnazbvijb6mkj4i .popup-inner,
#popup-harphome .popup-inner {
	width: 795px;
	height: 502px;
}
#popup-qwnazbvijb6mkj4i .popup-inner .modal-content,
#popup-harphome .popup-inner .modal-content {
	width: 795px;
	height: 502px;
}
#element-popup_qwnazbvijb6mkj4i_0 {
	height: 292px;
	width: 324px;
	left: 6px;
	top: 122px;
	z-index: 4;
}
#element-popup_qwnazbvijb6mkj4i_1 {
	height: 72px;
	width: 370px;
	left: 369px;
	top: 71px;
	z-index: 5;
}
#element-popup_qwnazbvijb6mkj4i_1 p, #element-popup_qwnazbvijb6mkj4i_1 ul, #element-popup_qwnazbvijb6mkj4i_1 ol, #element-popup_qwnazbvijb6mkj4i_1 h1 {
	font-size: 26px;
	line-height: 36px;
}
#element-popup_qwnazbvijb6mkj4i_2 {
	height: 25px;
	width: 571px;
	left: 122px;
	top: 26px;
	z-index: 6;
}
#element-popup_qwnazbvijb6mkj4i_2 p, #element-popup_qwnazbvijb6mkj4i_2 ul, #element-popup_qwnazbvijb6mkj4i_2 ol, #element-popup_qwnazbvijb6mkj4i_2 h1 {
	font-size: 18px;
	line-height: 25px;
}
#element-popup_qwnazbvijb6mkj4i_3 {
	height: 200px;
	width: 340px;
	left: 393px;
	top: 151px;
	z-index: 7;
}
.modal {
    overflow-x: hidden;
    overflow-y: auto;
}
.modal {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 999998;
    display: none;
    overflow: hidden;
    outline: 0;
}
.page-element {
    position: absolute;
}
#element-popup_qwnazbvijb6mkj4i_1 {
    height: 72px;
    width: 370px;
    left: 369px;
    top: 71px;
    z-index: 5;
}
#element-popup_qwnazbvijb6mkj4i_2 {
    height: 25px;
    width: 571px;
    left: 122px;
    top: 26px;
    z-index: 6;
}
#element-popup_qwnazbvijb6mkj4i_3 {
    height: 200px;
    width: 340px;
    left: 393px;
    top: 151px;
    z-index: 7;
}
.contents .field-text input {
    border: 1px solid #8095a8;
    padding: 10px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    background-color: #fff;
    margin: 8px 0 8px 0 !important;
    font-size: 15px;
    line-height: 19px;
    width: 100%;
    height: 43px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
#element-popup_qwnazbvijb6mkj4i_2 strong {
    font-size: 26px;
}
a#pop-close {
    cursor: pointer;
}
input {
    color: #000;
    webkit-appearance: none;
    -webkit-rtl-ordering: logical;
    -webkit-user-select: text;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: rgb(255, 255, 255);
    background-image: none;
    background-origin: padding-box;
    background-size: auto;
    border-bottom-color: rgb(0, 195, 243);
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-image-outset: 0px;
    border-image-repeat: stretch;
    border-image-slice: 100%;
    border-image-source: none;
    border-image-width: 1;
    border-left-color: rgb(0, 195, 243);
    border-left-style: solid;
    border-left-width: 1px;
    border-right-color: rgb(0, 195, 243);
    border-right-style: solid;
    border-right-width: 1px;
    border-top-color: rgb(0, 195, 243);
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    border-top-style: solid;
    border-top-width: 1px;
    box-sizing: border-box;
    color: rgb(89, 89, 89);
    cursor: auto;
    display: inline-block;
    font-family: sans-serif;
    font-size: 20px;
    font-stretch: normal;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    height: 54px;
    letter-spacing: normal;
    line-height: 19px;
    margin-bottom: 0px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    padding-bottom: 10px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 10px;
    text-align: start;
    text-indent: 0px;
    text-rendering: auto;
    text-shadow: none;
    text-transform: none;
    vertical-align: middle;
    width: 100%;
    word-spacing: 0px;
}
button, input, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
input[type="text"], input[type="password"], input[type="email"], textarea {
    color: #000000;
    background-color: #ffffff;
    border-color: #53b624;
}
.dynamic-button, .image-button
{
margin: 0;
    width: 327px;
    height: 59px;
    box-sizing: content-box;
    line-height: 24px;
    font-size: 20px;
}
.submit-button
{
    -webkit-appearance: none;
    -webkit-font-smoothing: antialiased;
    align-items: flex-start;
    background-color: rgb(0, 155, 193);
    background-image: none;
    border-bottom-color: rgb(255, 255, 255);
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    border-bottom-style: none;
    border-bottom-width: 0px;
    border-image-outset: 0px;
    border-image-repeat: stretch;
    border-image-slice: 100%;
    border-image-source: none;
    border-image-width: 1;
    border-left-color: rgb(255, 255, 255);
    border-left-style: none;
    border-left-width: 0px;
    border-right-color: rgb(255, 255, 255);
    border-right-style: none;
    border-right-width: 0px;
    border-top-color: rgb(255, 255, 255);
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    border-top-style: none;
    border-top-width: 0px;
    box-sizing: content-box;
    color: rgb(255, 255, 255);
    cursor: pointer;
    display: block;
    font-family: Arial;
    font-size: 20px;
    font-stretch: normal;
    font-style: normal;
    font-variant: normal;
    font-weight: bold;
    height: 59px;
    left: 17px;
    letter-spacing: normal;
    line-height: 24px;
    margin-bottom: 0px;
    margin-left: 0px;
    margin-right: 0px;
    margin-top: 0px;
    outline-color: rgb(255, 255, 255);
    outline-style: none;
    outline-width: 0px;
    overflow-x: visible;
    overflow-y: visible;
    padding-bottom: 0px;
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    position: absolute;
    text-align: center;
    text-decoration: none;
    text-indent: 0px;
    text-rendering: auto;
    text-shadow: none;
    text-transform: none;
    background-color: #00c3f3;
    color: #ffffff;
}
b#mf150 {
    font-family: "PT Sans", Arial, Helvetica, sans-serif;
}
/**********end tellmemore popup css*****************/
</style>
</head>
<body>
<header>
        <!-- top header -->
        <div class="container clearfix">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="mobile-logo text-center">
				<img src="<?php echo get_template_directory_uri(); ?>/reverseimages/logo.png" alt="logo">
			</div>
            <div class="row">
	            <div class="header-left col-sm-6 col-xs-6">
	                <div class="phoneNumber">
	                <img src="<?php echo get_template_directory_uri(); ?>/reverseimages/girl.png" alt="">
	                	<span>1-800-796-7371</span>
	                </div>
	            </div>
	            <div class="header_right pull-right col-sm-6 text-right col-xs-6">
	                <div class="topSocial">
	                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/tw.png" alt="Twitter"></a>
	                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/fb.png" alt="facebook"></a>
	                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/pint.png" alt="pinit"></a>
	                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/g+.png" alt="google+"></a>
	                </div>
	            </div>
            </div>
    	</div>
    	<!-- /.container -->
</header>
<!-- /.header -->

<!-- banner sec -->
<section class="banner-sec">
    <img src="<?php echo get_template_directory_uri(); ?>/reverseimages/home-banner.jpg" alt="home-banner">
    <div class="logo">
		<div class="container">
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/logo.png" alt="logo"></a>
		</div>
	</div>
</section>
<!-- / banner sec -->

<!-- middle sec -->
<section class="middle-sec">
	<div class="container">
		<div class="middle-sec-inner">
		        <h2 class="over62">Over 62?</h2>
				<div class="how-it-works green-line" style="">
					<img class="desktop-line" src="<?php echo get_template_directory_uri(); ?>/reverseimages/green-line.gif" alt="how it works">
					<img class="mobile-line" src="<?php echo get_template_directory_uri(); ?>/reverseimages/green-line2.png" alt="how it works">
				</div>
			<!-- middleSec one sec -->
			<div class="middleSecOne row">
				<div class="col-md-5 sec-left">
					<img src="<?php echo get_template_directory_uri(); ?>/reverseimages/stepone-img.png" alt="step1">

				</div>
				<div class="col-md-7 sec-right">
					<h2>Never make a Mortgage Payment Again <span>Get Cash for any Purpose you Choose</span> Get a Credit Line to Use as you Wish</h2>
				</div>
			</div>
			<!-- / middleSec one sec -->

			<!-- middleSec two sec -->
			<div class="middleSecTwo row">	
				<div class="col-md-5 sec-right text-right pull-right">
					<img src="<?php echo get_template_directory_uri(); ?>/reverseimages/steptwo-img.png" alt="step2">

				</div>			
				<div class="col-md-7 sec-left text-center">
					<h2>Unlock Your Equity with this FHA Insured <br>Government Guaranteed<span>Reverse Mortgage</span></h2>
					<a href="#" class="tellMeMore btn">Tell Me More</a>
				</div>
				
			</div>
            <!-- / middleSec two sec -->

			<!-- middleSecthree sec -->
			<div class="middleSecthree row">
				<div class="col-md-5 sec-left">
					<img src="<?php echo get_template_directory_uri(); ?>/reverseimages/stepthree-img.png" alt="step1">

				</div>
				<div class="col-md-7 sec-right">
					<h2>Over 3.5 Million Seniors Retain ownership of their homeWhile Enjoying the Financial Benefits it Provides.</h2>
					<ul>
						<li><span>*</span> Home Improvements</li>
						<li><span>*</span> Family Needs</li>
						<li><span>*</span> Enjoyment of Life</li>
					</ul>
					<a href="#" class="tellMeMore btn">Tell Me More</a>
				</div>
			</div>
			<!-- / middleSecthree sec -->
		</div>
		<!-- / Middle inner sec -->

		<!-- form sec -->
		<div class="formWrap">  <!-- form sec -->
			<h2>Instantly Receive Your Free Video</h2>
			<div class="form-inner">
				<div class="phone_number">1-800-796-7371</div>
				<form method="post" action="https://financeofamericamortgages.com/reverse-submit/">
			  		<!-- <input onfocus="if (this.value=='First Name') this.value = ''" onblur="if (this.value=='') this.value = 'First Name'" type="text" class="form-control" id="fname" value="First Name" required="required">
			  		<input onfocus="if (this.value=='Last Name') this.value = ''" onblur="if (this.value=='') this.value = 'Last Name'" type="text" class="form-control" id="lname" value="Last Name">
				    <input onfocus="if (this.value=='Email') this.value = ''" onblur="if (this.value=='') this.value = 'Email'" type="email" class="form-control" id="email" value="Email" required="required">
				    <input onfocus="if (this.value=='Phone Number') this.value = ''" onblur="if (this.value=='') this.value = 'Phone Number'" type="text" class="form-control" id="phohe" value="Phone Number" required="required">	 -->			    
				   <!--  <a href="thankyou.html" class="btn btn-default">Watch Video</a> -->
				   <!--  <button class="btn btn-default">Watch Video</button> -->

				    <input placeholder='First Name *' type="text" class="form-control" id="fname" required="required" name="firstname">
				    <input placeholder='Last Name' type="text" class="form-control" id="lname" name="lastname">
				    <input placeholder='Email *' type="email" class="form-control" id="email" required="required" name="emailid">
				    <input placeholder='Phone Number *' type="text" class="form-control" id="phohe" required="required" name="phone">	    
				    <!-- <a href="thankyou.html" class="btn btn-default">Watch Video</a> -->
				   <input type="submit" class="btn btn-default" value="Watch Video">
				</form>
			</div>
		</div>
		<!-- / form sec -->

	</div>
	<div class="reveseBottomSec text-center"> <!-- reveseBottomSec sec -->
		<div class="container">
			<h2>What is a Revese Mortgage?</h2>
			<p>	A Reverse Mortgage is a Mortgage that allows homeowners who are 62 or older to utilize the equity of their home to either get cash and or defer all of their mortgage payments . Essentially, if a homeowner “takes out” a Reverse Mortgage, he or she never makes a mortgage payment again. If there is enough equity, that equity may be turned into cash either as a lump sum or a Home Equity Line of Credit that never needs to be repaid. There is no need to credit or income qualify. The only qualification is that the homeowner MUST BE 62 years old or older and must have equity in the home.</p>
		</div>
	</div>
</section>
<!-- / middle sec -->
<footer>
	<div class="container">
	<div class="row">
		<div class="col-lg-7">
			<ul class="footer_nav">
				<li><a href="#" data-toggle="modal" data-target="#myModal">Privacy Policy</a></li>
					<li><a href="#"  data-toggle="modal" data-target="#myModa">Disclosures</a></li>
					<li><a href="#" data-toggle="modal" data-target="#myMod">Terms of Use</a></li>
					<li><a href="#" data-toggle="modal" data-target="#myLicensing">Licensing</a></li>
<li><a href="#"  data-toggle="modal" data-target="#myContactModal">Contact Us</a></li>
			</ul>                           
		</div>
		<div class="col-lg-5">
			<div class="topSocial">
                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/tw.png" alt="facebook"></a>
                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/fb.png" alt="facebook"></a>
                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/pint.png" alt="facebook"></a>
                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/reverseimages/g+.png" alt="facebook"></a>
            </div>
			<p class="copyright">&copy; 2014 Kick Assistant LLC.</p>
		</div>
	</div>
	</div>
</footer>
<!-- Modal -->
<div id="myContactModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
     
      </div>
      <div class="modal-body">
                <h3>Contact Us</h3>
 
<p>Finance of America Mortgage<br/>
1011 Warrenville Road Suite 325<br/>
 Lisle, IL 60532<br/>
800-796-7371<br/>
                </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>  

     <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
     
      </div>
      <div class="modal-body">
        <p>Finance of America takes your online privacy seriously. This disclosure is one way of maintaining your trust in our company, our products and services. Our privacy statement describes how we collect, utilize and protect information about you during your visit on our web site.</br>
<a href="#"> What Personal Information Does Finance of America Collect Online?</a><a href="#">  Why Is Personal Information Collected?</a><a href="#">  When Is Personal Information About Me Collected Online?</a><a href="#">  Does Finance of America Use Cookies or Other Online Technologies to Collect Information About Me?</a><a href="#">  Do You Share Personal Information About Me?</a><a href="#">  How Can I Review, Change or Correct Information You Collect?</a><a href="#">  How Do You Protect Personal Information?</a><a href="#">  How Do I Contact Finance of America If I Have Questions About This Privacy Statement? </a><a href="#"> Will Finance of America Make Changes To This Privacy Statement?</a>
<h3>What Personal Information Does Finance of America Collect Online?</h3>

We may collect personal information about you from the information you provide to us when you fill out an application or other forms on our site. We may also receive personal information about you from consumer reporting agencies, our affiliates or others. This information may include:
<ul><li>Financial Information</li>
<li>Name</li>
<li>Address</li>
<li>Social Security Number</li>
<li>Date of Birth</li>
<li>Account Numbers</li>
<li>Telephone Number</li>
<li>Email Address</li></ul>
<h3>Why Is Personal Information Collected?</h3>

If you fill out an application or other form on our site, we may ask for a variety of information to help us communicate with you, evaluate your eligibility and the product or services that may be right for you. We may also obtain other information about you, for example, your credit report. This information is used to determine your eligibility for our products and services. Also, if you choose to share any personal information with us, we may store it and use it for our own marketing research and the marketing of our products and services to you.
<h3>When Is Personal Information About Me Collected Online?</h3>

We collect personal information about you when you fill out an application or other forms on our website.
<h3>Does Finance of America Use Cookies or Other Online Technologies to Collect Information About Me?</h3>

Some of our web sites may make use of "cookie" technology to measure site activity and to customize information to your personal tastes. A cookie is an element of data that a web site can send to your browser, which may then store the cookie on your hard drive. So, when you come back to visit us again, we can tailor information to suit your individual preferences. The goal is to save you time and provide you with a more meaningful visit. When you visit our site, we may collect and store information about your visit. This information may include the time and length of your visit, the pages you view on our site, the last site you visited before coming to ours, and the name of your internet service provider. We use this data on an aggregate basis to measure site activity, and on an individual basis to make the site more useful and provide information that may be of interest to you.
<h3>Do You Share Personal Information About Me?</h3>

In certain circumstances, we may share information about you with other companies so that we may provide you with the products and services you have requested. We may share information about you with our suppliers and service providers for their use in providing services, or within the Finance of America family for market research and marketing purposes as allowed by law. Also, we may share information about you with credit reporting agencies when you complete an online form or application. Any company with which we share information about you for the above purposes is contractually required to comply with confidentiality standards. We may disclose information about you as required or permitted by law. We do not sell information about you to anyone.
<h3>How Can I Review, Change or Correct Information You Collect?</h3>

We are committed to maintaining accurate and up-to-date information on all of our customers. We may provide you with access to account information in different ways, for example, over the telephone, online or on paper. If you are a current customer, you may be able to update information such as: your address, telephone number and email address. You may contact us in order to do so. If you have completed and saved, but have not submitted, an online form or application you may be able to make certain changes to the information you have provided to us before it is submitted. If you are unable to make changes to the information you have provided to us online, you may contact us in order to do so.
<h3>How Do You Protect Personal Information?</h3>

We restrict access to the information obtained from our websites and web pages to our employees, agents and contractors. We maintain physical, electronic and procedural safeguards designed to protect personal information.
<h3>How Do I Contact Finance of America If I Have Questions About This Privacy Statement?</h3>

U.S. Mail:</br>
Finance of America Mortgage 300 Welsh Road, Building 5 Horsham, PA 19044</br>
Phone:	(215) 591-0222</br>
Toll-Free:	(800) 355-LOAN (5626)</br>
Fax:	(215) 591-0221</br>
email:	privacy@financeofamerica.com</br>
<h3>Will Finance of America Mortgage Make Changes To This Privacy Statement?</h3>

This statement is effective June 1, 2015 and remains in effect until amended. Finance of America Mortgage reserves the right to amend this Privacy Statement or alter its privacy principles at its sole discretion without notice to you. Any alterations to this Privacy Statement or our privacy principles will be posted on our web site in a timely manner.
<h3>Other Important Information</h3>

<strong>For California Consumers:</strong> In accordance with California law, we will not share information we collect about you with nonaffiliated third parties, except as permitted by law. For example, we may share information with your consent or as necessary to service or process a financial product or service that you request or authorize. We will also limit the sharing of information about you with our affiliates to the extent required by California law. For Vermont Residents: In accordance with Vermont law, we will not share information we collect about you with nonaffiliated third parties, except as permitted by law. For example, we may share information with your consent or as necessary to service or process a financial product or service that you request or authorize. We will not share information about your creditworthiness with our affiliates except with your authorization or consent.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>  


<!-- Modal -->
<div id="myModa" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
     
      </div>
      <div class="modal-body">
 <p>
<h3>Equal Housing Opportunity Lender</h3>

We conduct business in accordance with the Federal Fair Housing Law (Fair Housing Amendments Act of 1988). It is illegal to discriminate against any person because of race, color, religion, sex, handicap, familial status, or national origin:</br>
1. In the sale or rental of housing</br>
2. In advertising the sale or rental of housing</br>
3. In the financing of housing</br>
4. In the provision of real estate brokerage service</br>
5. In the appraisal of housing</br>
6. Blockbusting is also illegal</br>
Anyone who feels he or she has been discriminated against may file a complaint of housing discrimination: 1-800-669-9777 (toll free), or 1-800-927-9275 (tdd), U.S. Department of Housing and Urban Development Assistant Secretary for Fair Housing and Equal Opportunity, Washington DC 20410.</br>
<h3>Declaration of Fair Lending Principles and Practices</3>

<h4>Corporate Mission</h4>

Finance of America is committed to providing extraordinary customer service by making our customers' satisfaction the top priority. Finance of America is also dedicated to delivering quality mortgage solutions that support you in Opening Doors to Home Ownership.
<h4>Social Responsibility / Community Involvement</h4>

As a nationwide mortgage lender, we are committed to increasing affordable housing opportunities and addressing obstacles that face today's homebuyers, by providing products and programs that help bring home ownership to those who are under served. We also make contributions to local community causes with an emphasis on those in which our employees are involved.
<h4>Fair Lending Advancement</h4>

Finance of America is committed to the principle of providing access to mortgage credit for all people, regardless of race, color, religion, national origin, age (provided the applicant has the capacity to enter into a binding contract), sex, handicap, familial or marital status, the exercise of any right under the Consumer Credit Protection Act or the fact that all or part of the applicant's income derives from any public assistance program, and regardless of where the applicant resides, will reside, or where the residential property to be mortgaged is located. Consistent with these principles, Finance of America markets its loan products nationwide. It is Finance of America's policy and expectation that every loan applicant will first be considered for conforming conventional loan programs or government loan programs, where available, unless the applicant requests consideration of other programs. In addition an alternative loan product may be offered or recommended based upon the applicant's credit history and the interest rate desired, as well as the terms and conditions of the loan. Finance of America may engage in advertising campaigns or targeted solicitations with respect to alternative loans. In addition to maintaining compliance with applicable fair housing laws and regulations, Finance of America is committed to taking a leadership role in the lending community, by supporting affordable housing programs which benefit our communities and contribute towards helping people finance their home ownership dreams. We realize that participating in such programs not only benefits society as a whole but contributes to the continued viability and financial well being of Finance of America. Finance of America is committed to maintaining a work environment that is free from discrimination and encourages fair lending.
<h4>Fair Lending Housing Initiatives & Community Involvement</h4>

In order to promote home ownership, lenders must reach out to members of the communities they serve with information about the home buying process. In this regard, Finance of America Mortgage:</br>
Seeks to develop productive relationships, including participation in housing fairs with non-profit housing groups, counseling organizations and other community organizations.</br>
Conducts and participates in seminars for consumers, real estate brokers, mortgage brokers and other individuals and organizations in order to increase home ownership opportunities.</br>
Participates when economically feasible in public and private affordable housing and loan guarantee programs, such as those offered by the Federal Housing Administration (FHA) and State Housing Finance Agencies, in order to increase market share among under served populations.</br>
Assists in the development and promotion of affordable housing loan programs and products.</br>
<h3>Patriot Disclosure Act</h3>

<h4>Important Information About Procedures For Obtaining A Mortgage Loan</h4>

To help prevent fraud and to assist the government in fighting the funding of terrorism and money laundering activities, we are obtaining, verifying, and recording information that identifies each person who obtains a mortgage loan with us. What this means for you: When you obtain a mortgage loan, we will ask for your name, address, date of birth, and other information that will allow us to identify you. We may also ask to see your driver's license or other identifying documents.
<h3>State Specific Information/Disclosure</h3>

<h4>Texas Only</h4>

Complaints regarding mortgage bankers should be sent to the Department of Savings and Mortgage Lending, 2501 North Lamar, Suite 201, Austin, Texas 78705. A toll-free consumer hotline is available at 1-877-276-5550.</p>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
</div>
  </div>
</div>




<!-- Modal -->
<div id="myMod" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
     <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
     
      </div>
      <div class="modal-body">
        <p>Please read this agreement ("Agreement") carefully before accessing or using this web site ("Site"). By accessing or using the Site, you agree to be bound by this Agreement. Finance of America and its suppliers provide the information and services on this Site to you, the user, conditioned on your acceptance without modification of the terms, conditions and notices contained herein. Your use of this site constitutes your agreement to all such terms, conditions and notices. The information and services offered on this Site are provided with the understanding that neither Finance of America, nor its suppliers or users are engaged in rendering legal or other professional services or advice. Your use of the Site is subject to the additional disclaimers and caveats that may appear throughout the Site. Finance of America and its agents assume no responsibility for any consequence relating directly or indirectly to any action or inaction that you take based on the information, services or other material on this Site. While Finance of America and its suppliers, strive to keep the information on this Site accurate, complete and up-to-date, Finance of America, and its suppliers cannot guarantee, and will not be responsible for any damage or loss related to, the accuracy, completeness or timeliness of the information.
<h3>Personal And Noncommercial Use Limitation: Prohibited Uses</h3>

You may access, download and print materials on this Site for your personal and non-commercial use. You may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, frame in another Web page, use on any other web site, transfer or sell any information, software, lists of users, databases or other lists, products or services obtained from this Site. The foregoing prohibition expressly includes, but is not limited to, the practices of "screen scraping" or "database scraping" to obtain lists of users or other information. If and when requested by Finance of America, you agree to provide true, accurate and complete user information and to refrain from impersonating or falsely representing your affiliation with any person or entity. Except with the written permission of Finance of America, you agree to refrain from accessing or attempting to access password protected, secure or non-public areas of this Site. Unauthorized individuals attempting to access prohibited areas of this Site may be subject to prosecution.
<h3>Proprietary Rights</h3>

All materials on this Site (as well as the organization and layout of the Site) are owned and copyrighted or licensed by Finance of America Mortgage. Copyright Finance of America Mortgage, 2006. All rights reserved. Reproduction, distribution, or transmission of the copyrighted materials contained within this Site is strictly prohibited without the express written permission of Finance of America.
<h3>No Unlawful Or Prohibited Use</h3>

As a condition of your use of this Site, you warrant to Finance of America, that you will not use this Site for any purpose that is unlawful or prohibited by these terms, conditions, and notices. If you violate any of these terms, your permission to use the Site automatically terminates.
<h3>Liability Disclaimer</h3>

Use of this Site is at your own risk. If your use of this Site or the materials therein results in the need for servicing or replacing property, material, equipment or data, neither Finance of America, nor its affiliates are responsible for those costs. THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES PUBLISHED ON THIS SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. Finance of America, ITS AFFILIATES AND/OR ITS RESPECTIVE SUPPLIERS MAY WITHOUT PRIOR NOTICE MAKE IMPROVEMENTS AND/OR CHANGES IN THIS SITE INCLUDING BUT NOT LIMITED TO THE INFORMATION, SERVICES, PRODUCTS OR OTHER MATERIAL AT ANY TIME. ALL INFORMATION, PRODUCTS, AND SERVICES ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND. IN NO EVENT SHALL Finance of America, ITS AFFILIATES AND/OR ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF THIS SITE OR WITH THE DELAY OR INABILITY TO USE THIS SITE, OR FOR ANY INFORMATION, PRODUCTS, MATERIAL AND/OR SERVICES OBTAINED THROUGH THIS SITE, OR OTHERWISE ARISING OUT OF THE USE OF THIS SITE, WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, EVEN IF Finance of America, ITS AFFILIATES OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU.
<h3>Indemnification</h3>

You agree to indemnify, defend and hold harmless Finance of America, its affiliates and suppliers from any liability, loss, claim and expense (including attorney's reasonable fees) related to your violation of this Agreement.
<h3>Links To Third Party Sites</h3>

Outside Links - While on this web site, you may be led to other web sites operated by parties other than Finance of America, or its affiliates. Inclusion of hyperlinks by Finance of America, to web sites does not imply any endorsement of the material on such web sites or any association with their operators, and you access and use such sites, including information, material, products and services therein, solely at your own risk. We are not responsible for the quality or results or any product or service provided by these companies. Review their products and services carefully. Furthermore, because the privacy policy you just read is applicable only when you are on our Site, once linked to another web site, you should read that site's privacy policy before disclosing any personal information. Their own privacy policies may be different than ours. We are not responsible for the use of the information that you give these third parties.
<h3>Changes To Agreement</h3>

Finance of America may modify this Agreement at any time, and such modifications shall be effective immediately upon posting of the modified Agreement. Accordingly, you agree to review the Agreement periodically, and your continued access or use of this Site shall be deemed your acceptance of the modified Agreement.
<h3>Miscellaneous</h3>

This website is applicable to and should only be used by persons 18 years of age or older located within the United States of America. Access and use of this website by persons under the age of 18 or from locations other than the United States is prohibited. This Agreement and the resolution of any dispute related to this Agreement or this Site shall be governed by and construed in accordance with the laws of Pennsylvania without giving effect to any principles of conflicts of law. Failure by Finance of America or its affiliates to insist upon strict enforcement of any provision of this Agreement shall not be construed as a waiver of any provision or right. You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of this Site or this Agreement must be filed within one (1) year after such claim or cause of action arose or be forever barred. Any legal action or proceeding between Finance of America, and/or its affiliates and you related to this Agreement shall be brought exclusively in federal district court.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myLicensing" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
     
      </div>
      <div class="modal-body">
        <p><div class="modal-body">
<h2>Licensing Information <span style="font-size: 15px"><a href="http://www.nmlsconsumeraccess.org/EntityDetails.aspx/COMPANY/1071" target="_blank">(click here for Nationwide Mortgage Lending System)</a></span></h2>

<hr>

<table>
<tbody>
<tr>
<td>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="col-md-12" colspan="3" valign="bottom">Finance of America Mortgage, LLC - NMLS #1071</td>
</tr>
<tr>
<td class="col-md-12" colspan="3" valign="bottom"></td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">AL</td>
<td class="col-md-6" valign="bottom">AL Consumer Credit License</td>
<td class="col-md-4" valign="bottom">(#MC4649)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">AZ</td>
<td class="col-md-6" valign="bottom">Licensed Mortgage Banker AZ</td>
<td class="col-md-4" valign="bottom">(#BK0910184)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">AR</td>
<td class="col-md-6" valign="bottom">Licensed Combination Mortgage Banker-Broker-Servicer AR</td>
<td class="col-md-4" valign="bottom">(#11772)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">CA</td>
<td class="col-md-6" valign="bottom">CA Finance Lenders Law License</td>
<td class="col-md-4" valign="bottom">(#603E024)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">CA</td>
<td class="col-md-6" valign="bottom">CA Residential Mortgage Lending Act License</td>
<td class="col-md-4" valign="bottom">(#4130675)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">CO</td>
<td class="col-md-6" valign="bottom">Licensed Mortgage Broker in CO</td>
<td class="col-md-4" valign="bottom"></td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">CT</td>
<td class="col-md-6" valign="bottom">CT Mortgage Lending License</td>
<td class="col-md-4" valign="bottom">(#11040)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">DE</td>
<td class="col-md-6" valign="bottom">Licensed by the Delaware Office of the State Bank Commissioner</td>
<td class="col-md-4" valign="bottom">(#2299)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">DC</td>
<td class="col-md-6" valign="bottom">District of Columbia Mortgage Dual Authority License</td>
<td class="col-md-4" valign="bottom">(#MLB1071)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">FL</td>
<td class="col-md-6" valign="bottom">Licensed Correspondent Mortgage Lender FL</td>
<td class="col-md-4" valign="bottom">(#CL0701952)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">GA</td>
<td class="col-md-6" valign="bottom">GA Mortgage Lender License</td>
<td class="col-md-4" valign="bottom">(#15499)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">ID</td>
<td class="col-md-6" valign="bottom">ID Mortgage Broker/Lender License</td>
<td class="col-md-4" valign="bottom">(#MBL-6051)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">IL</td>
<td class="col-md-6" valign="bottom">IL Residential Mortgage License</td>
<td class="col-md-4" valign="bottom">(#MB.0005883)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">IN</td>
<td class="col-md-6" valign="bottom">IN First Lien Mortgage Lending License</td>
<td class="col-md-4" valign="bottom">(#11220)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">IA</td>
<td class="col-md-6" valign="bottom">IA Mortgage Banker License</td>
<td class="col-md-4" valign="bottom">(#2009-0031)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">KS</td>
<td class="col-md-6" valign="bottom">KS Mortgage Company License</td>
<td class="col-md-4" valign="bottom">(#MC.0001491)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">MD</td>
<td class="col-md-6" valign="bottom">MD Mortgage Lender License</td>
<td class="col-md-4" valign="bottom">(#5769)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">MA</td>
<td class="col-md-6" valign="bottom">MA Mortgage Broker License</td>
<td class="col-md-4" valign="bottom">(#MC1071)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">MA</td>
<td class="col-md-6" valign="bottom">MA Mortgage Lender License</td>
<td class="col-md-4" valign="bottom">(#MC1071)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">ME</td>
<td class="col-md-6" valign="bottom">ME Supervised Lender License</td>
<td class="col-md-4" valign="bottom">(#SLB6718)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">MI</td>
<td class="col-md-6" valign="bottom">MI 1st Mortgage Broker/Lender Registrant</td>
<td class="col-md-4" valign="bottom">(#FR0016592)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">MI</td>
<td class="col-md-6" valign="bottom">MI 2nd Mortgage Broker/Lender Registrant</td>
<td class="col-md-4" valign="bottom">(#SR0016593)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">MN</td>
<td class="col-md-6" valign="bottom">MN Residential Mortgage Originator License</td>
<td class="col-md-4" valign="bottom">(#MN-MO-40196588)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">NH</td>
<td class="col-md-6" valign="bottom">Licensed by the New Hampshire banking department</td>
<td class="col-md-4" valign="bottom">(#10192-MB)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">NJ</td>
<td class="col-md-6" valign="bottom">NJ Residential Mortgage Lender License</td>
<td class="col-md-4" valign="bottom">(#9601232)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">NM</td>
<td class="col-md-6" valign="bottom">NM Mortgage Loan Company License</td>
<td class="col-md-4" valign="bottom">(#02188)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">NY</td>
<td class="col-md-6" valign="bottom">Website authorization by the New York State Department of Financial Services is pending. Until this website is authorized, no mortgage loan applications for properties located in New York will be accepted through this site. Licensed Mortgage Banker - NYS Department of Financial Services</td>
<td class="col-md-4" valign="bottom">(#B500791)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">NC</td>
<td class="col-md-6" valign="bottom">NC Mortgage Lender License</td>
<td class="col-md-4" valign="bottom">(#L-121400)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">OH</td>
<td class="col-md-6" valign="bottom">OH Mortgage Broker Act Mortgage Banker Exemption</td>
<td class="col-md-4" valign="bottom">(#MBMB.850078.000)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">OH</td>
<td class="col-md-6" valign="bottom">OH Mortgage Loan Act Certificate of Registration</td>
<td class="col-md-4" valign="bottom">(#SM.501717.000)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">OK</td>
<td class="col-md-6" valign="bottom">OK Mortgage Broker License</td>
<td class="col-md-4" valign="bottom">(#MB001315)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">OR</td>
<td class="col-md-6" valign="bottom">OR Mortgage Lending License</td>
<td class="col-md-4" valign="bottom">(#ML-3443)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">PA</td>
<td class="col-md-6" valign="bottom">PA Mortgage Lender license</td>
<td class="col-md-4" valign="bottom">(#21474)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">RI</td>
<td class="col-md-6" valign="bottom">RI Lender License</td>
<td class="col-md-4" valign="bottom">(#20041625LL)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">RI</td>
<td class="col-md-6" valign="bottom">RI Loan Broker License</td>
<td class="col-md-4" valign="bottom">(#20041626LB)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">SC</td>
<td class="col-md-6" valign="bottom">SC-BFI Mortgage Lender/Servicer License</td>
<td class="col-md-4" valign="bottom">(#MLS - 1071)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">TN</td>
<td class="col-md-6" valign="bottom">TN Mortgage License</td>
<td class="col-md-4" valign="bottom">(#2782)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">TX</td>
<td class="col-md-6" valign="bottom">TX-SML Mortgage Banker Registration</td>
<td class="col-md-4" valign="bottom"></td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">UT</td>
<td class="col-md-6" valign="bottom">UT-DRE Mortgage Entity License</td>
<td class="col-md-4" valign="bottom">(#5608213)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">VY</td>
<td class="col-md-6" valign="bottom">VT Lender License</td>
<td class="col-md-4" valign="bottom">(#5465)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">VT</td>
<td class="col-md-6" valign="bottom">VT Mortgage Broker License</td>
<td class="col-md-4" valign="bottom">(#0485 MB)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">VA</td>
<td class="col-md-6" valign="bottom">Licensed by the Virginia State Corporation Commission</td>
<td class="col-md-4" valign="bottom">(#MC1772)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">WA</td>
<td class="col-md-6" valign="bottom">WA Consumer Loan Company License</td>
<td class="col-md-4" valign="bottom">(#CL-1071)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">WV</td>
<td class="col-md-6" valign="bottom">WV Mortgage Broker License</td>
<td class="col-md-4" valign="bottom">(#MB-21755)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">WV</td>
<td class="col-md-6" valign="bottom">WV Mortgage Lender License</td>
<td class="col-md-4" valign="bottom">(#ML-21756)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">WI</td>
<td class="col-md-6" valign="bottom">WI Mortgage Banker License</td>
<td class="col-md-4" valign="bottom">(#43680BA)</td>
</tr>
<tr>
<td class="col-md-2" valign="bottom">WI</td>
<td class="col-md-6" valign="bottom">WI Mortgage Broker License</td>
<td class="col-md-4" valign="bottom">(#28486BR)</td>
</tr>
<tr>
<td class="col-md-12" colspan="3" valign="bottom">This is not an offer to extend credit to any individual who may be entitled to a more complete disclosure per RESPA, TILA, HOEPA or any other more applicable federal, state or local law or regulation. Rates, Terms, Fees, Products, Programs and Equity requirements are subject to change without notice. For qualified borrowers only.</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--Modal POP up tell me more-->
<div id="popup-harphome" class="popup modal fade in" style="display:none;">
<div id="mf143" class="popup-inner modal-dialog modal-dialog-center" style="margin-top: 82.5px;">
<div id="mf144" class="modal-content">

<a id="popup-close" style="float: right; font-size: 18px; color: red; padding: 5px; font-weight: bold;" href="#">☓</a>
<div id="element-popup_qwnazbvijb6mkj4i_0" class="page-element widget-container page-element-type-image widget-image ">
<div id="mf145" class="contents"><img id="mf146" style="margin-left: 40px; height: 292px; width: 324px; margin-top: 0px;" src="https://financeofamericamortgages.com/wp-content/uploads/2016/01/oldpeople-300x197.jpg" alt="" /></div>
</div>
<form class=" margin-minus" action="https://financeofamericamortgages.com/reverse-submit/" method="post">
<div id="element-popup_qwnazbvijb6mkj4i_1" class="page-element widget-container page-element-type-headline widget-headline ">
<div id="mf147" class="contents" style="color: #000000;">
<p id="mf149" style="text-align: center; font-size: 26px; line-height: 36px;" draggable="false"><b id="mf150">
Never make a Mortgage Payment Again!
 </b></p>

</div>
</div>
<div id="element-popup_qwnazbvijb6mkj4i_2" class="page-element widget-container page-element-type-headline widget-headline ">
<div id="mf151" class="contents" style="color: #000000;">

<p>Watch Free Video</p>


</div>
</div>
<div id="element-popup_qwnazbvijb6mkj4i_3" class="page-element widget-container page-element-type-html widget-html ">
<div id="mf160" class="input-holder field-text">
<div id="mf161" class="field-element "><input id="first_name_pop" class="shortnice form-input first_name_pop" style="margin-bottom: 10px;" autocomplete="off" name="firstname" required="" type="text" value="" placeholder="First Name" /></div>
</div>
<div id="mf162" class="input-holder field-text">
<div id="mf163" class="field-element "><input id="last_name_pop" class="shortnice form-input last_name_pop" style="margin-bottom: 10px;" autocomplete="off" name="lastname" required="" type="text" value="" placeholder="Last Name" /></div>
</div>
<div id="mf166" class="input-holder field-text">
<div id="mf167" class="field-element "><input id="PhoneNumber_pop" class="shortnice form-input PhoneNumber_pop" autocomplete="off" name="phone" required="" type="text" value="" placeholder="Preferred Telephone Number" /></div>
</div>
<button id="mfi11" class="btn submit-button button_submit dynamic-button corners save-button checkbtn_pop" style="margin-left: -10px; margin-top: 10px;">Watch Free Video</button>
<div id="mf168" style="clear: both;"></div>
</div>
</form></div>
</div>
</div>
<!-- modal End -->
<!-- Scripts linking --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/reversejs/bootstrap.min.js"></script> 
  <script src="<?php echo get_template_directory_uri(); ?>/reversejs/img-scroll.js"></script>
<script async type="text/javascript" src="https://financeofamericamortgages.com/wp-content/themes/Avada/js/formatter.min.js"></script>
<script async type="text/javascript">
	$(window).load(function(){
		new Formatter(document.getElementById('phohe'), {
			'pattern': '({{999}})-{{999}}-{{9999}}'
		});
       });
 $(window).load(function(){
		new Formatter(document.getElementById('PhoneNumber_pop'), {
			'pattern': '({{999}})-{{999}}-{{9999}}'
		});
});
</script>
<script>
$("#pop-close").click(function(e){
            e.preventDefault();
    	    $( ".email-form" ).submit();
    	    $(".email-form").trigger("submit");
    	    $(".popup").hide();
        });
        
        $('.tellMeMore').on('click', function() {
            $('#popup-harphome').show();
        });
        
        $("#popup-close").click(function(e){
            e.preventDefault();
            $("#popup-harphome").hide();
        });
        </script>
</body>
</html>
