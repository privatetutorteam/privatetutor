<?php session_start();
	/*if(!isset($_SESSION['username']))
	{
	   header("location:?page_id=2020");
	   die();	
	}*/
/* Template Name: View Teacher Profile */
?>





<script src="<?php echo WP_SITEURL ?>wp-content/themes/tutor/js/prefixfree.min.js"></script>
<?php ob_start();?>
<?php get_header();?>

<link rel='stylesheet' href='<?php echo CHILD_URL; ?>/css/jquery-ui.min.css' />
<link href='<?php echo CHILD_URL; ?>/css/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo CHILD_URL; ?>/css/bootstrap.min.css' rel='stylesheet' />
  <link href='<?php echo CHILD_URL; ?>/css/bootstrap-timepicker.min.css' rel='stylesheet' />
 <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel='stylesheet' />
<script src='<?php echo CHILD_URL; ?>/js/moment.min.js'></script>
<script src='<?php echo CHILD_URL; ?>/js/jquery.min.js'></script>
<script src='<?php echo CHILD_URL; ?>/js/fullcalendar.min.js'></script>
<script src='<?php echo CHILD_URL; ?>/js/bootstrap.min.js'></script>
<script src='<?php echo CHILD_URL; ?>/js/bootstrap-timepicker.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
<script>

	$(document).ready(function() {
                $("#close").click(function(){
			$("#event-section").hide();
		});
                $(document).on("click",".fc-day-number",function(e){

                       var d = $(this).attr('data-date');


                        $("#event-section").show();
                        $('body').find(".event-date").val(d);

 		});

 		$('#event-subject').on('change', function (e) {
               var optionSelected = $("option:selected", this);
               var valueSelected = this.value;
               if(valueSelected == 'geography'){
                    $(".input-field").prop('disabled', false);
                    $('input[type="button"]').prop('disabled', false);

                } else{
                        var student_username = "<?php echo $_SESSION['username']; ?>";

                        $.ajax({
			type: "POST",
			url: "<?php echo WP_SITEURL; ?>check.php",
                        data: { student_username : student_username },
			dataType:"json",
			beforeSend: function(){console.log('Sending.......');},
                        success : function(data){ var credits = data.credits; if(credits!=1){ alert("You are not eligible to Create the Session , First Purchase the Credits"); }else{
                         
                         $(".input-field").prop('disabled', false);
                        $('input[type="button"]').prop('disabled', false);
                         
                         } 
                        },
			error:function(){console.log('There is some error occurred during processing request');}
	           });
               
   }            
});
 		
                window.onload = function() {
                   
               
		$('#calendar').fullCalendar({
		      
			theme: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '<?php echo date("Y/m/d"); ?>',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				<?php 
global $wpdb;
    $events = $wpdb->get_results($wpdb->prepare("SELECT * from wp_classes where teacher_id=%s",$_REQUEST["tutor_id"]));
	foreach ($events as $row ) :
	
 ?>
				{
					title: '<?php echo $row->class_title ;  ?>',
					start: '<?php echo  $row->start_date;  ?>'
				},
<?php endforeach; ?>
			]
		});
		}
	});
</script>

<script>
$(document).ready(function() {
$('#event-time').timepicker();
});
</script>


<?php //$tutor_id = $_REQUEST['tutor_id'];

	//Db Connection
	//mysql_connect("localhost","root","");
	//mysql_select_db("privatetutor");

	//$query = "SELECT * FROM register where Id = '$tutor_id'";
	//$results = $wpdb->
//get_results($query);
	//$things = array();
	//#print_r($results[0]);
	//foreach( $results[0] as $key => $result ) {
	//$things[$key] = $result;
	//}
	//?>
	<div class="content-main container">
		<div class="row">












    
          <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url("http://fonts.googleapis.com/css?family=Open+Sans:400,600,700");
				@import url("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css");
				*, *:before, *:after {
				  margin: 0;
				  padding: 0;
				  box-sizing: border-box;
				}

				html, body {
				  height: 100%;
				}

				body {
				  font: 14px/1 'Open Sans', sans-serif;
				  color: #555;
				  background: #eee;
				}

				h1 {
				  padding: 50px 0;
				  font-weight: 400;
				  text-align: center;
				}

				p {
				  margin: 0 0 20px;
				  line-height: 1.5;
				}

				section {
				  display: none;
				  padding: 20px 0 0;
				  border-top: 1px solid #ddd;
				}

				input {
				  display: none;
				}

				label {
				  display: inline-block;
				  margin: 0 0 -1px;
				  padding: 15px 25px;
				  font-weight: 600;
				  text-align: center;
				  color: #bbb;
				  border: 1px solid transparent;
				}

				label:before {
				  font-family: fontawesome;
				  font-weight: normal;
				  margin-right: 10px;
				}

				label:hover {
				  color: #888;
				  cursor: pointer;
				}

				input:checked + label {
				  color: #555;
				  border: 1px solid #ddd;
				  border-top: 2px solid orange;
				  border-bottom: 1px solid #fff;
				}

				#tab1:checked ~ #content1,
				#tab2:checked ~ #content2,
				#tab3:checked ~ #content3,
				#tab4:checked ~ #content4 {
				  display: block;
				}
				table tr th
				{
				 padding:10px 40px;
				}
				table tr td
				{
				 text-align:center;
				}
				@media screen and (max-width: 650px) {
				  label {
					font-size: 0;
				  }

				  label:before {
					margin: 0;
					font-size: 18px;
				  }
				}
				@media screen and (max-width: 400px) {
				  label {
					padding: 15px;
				  }
				}
				#dialog{
				display:none;
				background-color: blanchedalmond;
				color:blue;
				}

					</style>





  
 



<main>
  
  
  </section>
 
	  <input id="tab1" type="radio" name="tabs" <?php if($_GET['l']=='check'){echo checked;} else{} ?>>
  <label for="tab1">Events List</label>	


<div id="dialog" title="Basic dialog">
  <p>Please Login to see event list </p>
</div>

    
<input id="tab2" type="radio" name="tabs" <?php if($_GET['l']=='check'){} else{echo checked;} ?>>
  <label for="tab2">Profile</label>
    

  <input id="tab3" type="radio" name="tabs">
  <label for="tab3">Feedback</label>
   
    
  <section id="content1" >
		<div id="calendar" style="<?php if(!isset($_SESSION['username'])){echo "display:none"; } ?>"></div>
		
		<?php 
		if(!isset($_SESSION['username'])){
		
		echo '<h1 style="color:bluesky;">Please login to See Event List</h1>';
		}
		?>
  </section>
	<style type="text/css">
		.overall-rating{font-size: 14px;margin-top: 5px;color: #D83636;}
	</style>
	<?php
	include_once 'dbConfig.php';
	//Fetch rating deatails from database
	$tutor_id=$_REQUEST['tutor_id'];
	//$query = "SELECT rating_number, FORMAT((total_points / rating_number),1) as average_rating FROM post_rating WHERE tutor_id ='$tutor_id'  AND status = 1";
	$query = "SELECT SUM(rating_number)as rating_number,SUM(FORMAT((total_points / rating_number),1))/COUNT(rating_id) as average_rating FROM post_rating WHERE tutor_id ='$tutor_id'  AND status = 1";
	$result = $db->query($query);
	$ratingRow = $result->fetch_assoc();
	$rating=$ratingRow['average_rating'];
	?>
	<script>
		$(function() {
			var rate='<?php echo $rating; ?>';
			$("#rating_star").codexworld_rating_widget({
				starLength: '5',
				initialValue: +rate,

				imageDirectory: 'images/',
				inputAttr: 'postID'
			});
		});


	</script>


  <section id="content2" >
   
			<?php 
				$tutor_id = $_REQUEST['tutor_id'];
				global $wpdb;
				$post_id = $wpdb->get_results("SELECT * FROM register where Id='$tutor_id'");
				foreach($post_id[0] as $key=>$result){
					$things[$key] = $result;
				}
				
				?>
<div class="row back">

		<div class="span12">
		
        <div class="span-6" style="width:22%">
				<div class="profile-pic">
				
				<img class="thumb" src="<?php echo CHILD_URL.'/images/uploads/'.$things['profilepic']; ?>">
				
				</div>
			</div>
			<div class="span-6" style="width:72%">
				<div class="personal-info">	
					<div class="tutor-name"><?php echo $things['First_Name'].' '.$things['Last_Name']; ?></div>
					 
	    
					<div class="profile-rate"><div>
							<input name="rating" value="0" id="rating_star" type="hidden" postID="1" >

						</div></div>
					<div class="hourly-rating"><h6><img src="<?php echo CHILD_URL; ?>/images/Price.png">&nbsp<?php echo $things['hourlyrate']; ?> / h</h6></div>
			<div class="tutor-subject"><h6><img src="<?php echo CHILD_URL; ?>/images/skill.png">&nbsp;&nbsp;Skills : <?php //echo substr($key->Skills ,0,15);
                                       $a=explode(' ',$things['Skills']);
					echo $b=implode('/',$a);					
                     					?></h6></div>
					
					
				</div>
			</div>
                       </div></div>
                    <div id="other-info" style="">
				  <div id="other-info-heading"><h1> Tutor information</h1> </div>
				  <div id="left-content" style="">
				  <img  src="<?php echo CHILD_URL; ?>/images/general-info.png">
				  <label id="tutor-gen-info">General information:-</label>
				  <table id="tutor-gen-table">
				  <tr>
				  <td>Name :-</td>
				  <td><?php echo $things['First_Name'].' '.$things['Last_Name']; ?></td>
				  </tr>
				  <tr>
				  <td>Gender :-</td>
				  <td><?php echo $things['Gender']; ?></td>
				  </tr>
				  <tr>
				  <td>Contact Number :-</td>
				  <td><?php echo $things['Phone']; ?></td>
				  </tr>
				  <tr>
				  <td>Address :-</td>
				  <td><?php echo $things['Address']; ?></td>
				  </tr>
				  </table>
				  </div>
				  <div id="right-content">
				  <img  src="<?php echo CHILD_URL; ?>/images/about-me.png" id="img-about-me">
				  <label id="tutor-gen-info">About me:-</label>
				  <h3 class="welcome-tutor">Welcome,</h3><br>
				  <p id="about-tutor"><?php echo $things['About'];?></p>
				  
				  </div>
				  </div>
				  
  </section>
				  
  <section id="content3">
  
    
    
    
  </section>
    
</main>
		
		</div>
	</div>

	                            <?php //} 
	                            //}
	                            ?>

<!-----------End Profile Fetching -->
<!----------------Include Event Form -->
<div id="event-section"><?php include("event-form.php"); ?></div>



<?php get_footer(); ?>
	<script type="text/javascript" src="<?php echo CHILD_URL; ?>/js/ratingafter.js"></script>
	<link href="<?php echo CHILD_URL; ?>/css/rating.css" rel="stylesheet" type="text/css">
<?php ob_flush(); ?>