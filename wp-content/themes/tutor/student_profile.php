<?php ob_start(); ?>
<?php session_start();
if (!isset($_SESSION['username'])) {
    header("location:?page_id=2020");
    die();
}
?>
<?php /*
Template Name: Student Profile 
*/
?>
    <script src="<?php echo CHILD_URL; ?>/js/prefixfree.min.js"></script>

<?php get_header(); ?>
    <style type="text/css">
        #overlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: #000;
            filter: alpha(opacity=70);
            -moz-opacity: 0.7;
            -khtml-opacity: 0.7;
            opacity: 0.7;
            z-index: 100;
            display: none;
        }

        .contentss a {
            text-decoration: none;
        }

        .popup {
            width: 50%;
            margin: 0 auto;
            display: none;
            position: fixed;
            z-index: 101;
        }

        .contentss {
            margin: -500px 416px 0;
            max-width: 500px;
            min-width: 250px;
            width: 100%;
            padding: 10px 50px;
            border: 2px solid #808080;
            height: auto;
            border-radius: 10px;
            font-family: raleway;
            background-color: #F5F5F5;
            float: left;

            box-shadow: 0 2px 5px #000;
            color: #000;
            font-size: 16px;
        }

        .contentss p {
            clear: both;
            color: #555555;
            text-align: justify;
        }

        .contentss p a {
            color: #d91900;
            font-weight: bold;
        }

        .contentss .x {
            float: right;
            height: 35px;
            left: 22px;
            position: relative;
            top: -25px;
            width: 34px;
        }

        .content .x:hover {
            cursor: pointer;
        }

        .feedback_text {
            font-size: 20px;
            line-height: 2.5em;
            font-family: initial;
            color: rgb(18, 26, 26);
        }

        .give_feedback_text {
            font-size: 22px;
        }

        #feedbackss {
            resize: both;
            width:400px;
            height: 88px;

            font-size: 13px;
        }


        #p {

            cursor: pointer;
        }

        #container {
            height: 100%;
            width: 100%;
            background: #000;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
            position: fixed;
            z-index: 1000;
            opacity: 0.9;
            display: none;
            opacity: 0;
        }

        #popup {
            margin: 0px auto;
            margin-top: 200px ;
            height: 170px;
            width: 500px;
            top: 160px;
            right: 20px;
            left: 20px;
            bottom: 10px;
            position: absolute;
            z-index: 1500;
            box-shadow: 0 0 50px #000;
            display: none;
            border: 2px solid #B1A9A9;
            border-radius: 10px;
            background-color: #ffffff;
        }

        #label {
            cursor: pointer;
            color: aqua;
        }

        #div h3 {
            font: 28px/130px Roboto;
            color: #2D426F;
            text-align: center;
        }
    </style>
    <style type="text/css">
        .overall-rating {
            font-size: 14px;
            margin-top: 5px;
            color: #D83636;
        }

    .close {
    color: #000;
    float: right;
    font-size: 20px;
    font-weight: bold;
    line-height: 20px;
    margin-right: -40px;
    opacity: 0.53;
    text-shadow: 0 1px 0 #ffffff;
}
 .rating{
 float: left;
 width:auto;
 font-size: 26px;
 fomt-color:black;
}
.divLabel{
float: left;
font-color:black;
 font-size: 18px;
}
    </style>
<div class="loader"></div>

<?php $username = $_SESSION['username'];

//Db Connection
mysql_connect("localhost", "privatetutor", "sp");
mysql_select_db("privatetutor");
global $wpdb;
$query = "SELECT * FROM register_student where username = '$username'";
$results = $wpdb->get_results($query);
$things = array();
//print_r($results[0]);
foreach ($results[0] as $key => $result) {
    $things[$key] = $result;
}
?>
    <div class="content-main container">
        <div class="row">


            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

            <script>
                $(document).ready(function () {


                    $("#upcoming-event").click(function () {

                        $(".upcoming-event").show();
                        $(".live-event").hide();
                        $(".past-event").hide();
                        $(".expired-event").hide();
                    });
                    $("#live-event").click(function () {
                        $(".live-event").show();
                        $(".upcoming-event").hide();
                        $(".past-event").hide();
                        $(".expired-event").hide();
                    });

                    $("#past-event").click(function () {
                        $(".past-event").show();
                        $(".upcoming-event").hide();
                        $(".live-event").hide();
                        $(".expired-event").hide();
                    });

                    $("#expired-event").click(function () {
                        $(".expired-event").show();
                        $(".past-event").hide();
                        $(".upcoming-event").hide();
                        $(".live-event").hide();

                    });


                });


            </script>

            <script>
                $(document).ready(function () {

                    var session = "<?php echo $_SESSION['username']; ?>";
                    if (session) {
                        $("#content1").show();
                    } else {
                        $("#content1").hide();
                    }
            </script>

            <script>
                $(function () {
                    $("#rating_star").codexworld_rating_widget({
                        starLength: '5',
                        initialValue: '',
                        callbackFunctionName: 'processRating',
                        imageDirectory: 'images/',
                        inputAttr: 'postID'
                    });
                });

                function processRating(val, attrVal) {
                    var feedback = $('#feedbackss');
                    var z = "<?php echo CHILD_URL; ?>";
                    var stu_id="<?php echo $_REQUEST['stu_id']; ?>";
                    var t_id="<?php echo $_REQUEST['t_id']; ?>";

                    $.ajax({
                        type: 'POST',
                        url: z + '/rating.php',
                        data: 'postID=' + attrVal + '&ratingPoints=' + val + '&feedback=' + feedback.val() + '&stu_id=' + stu_id + '&t_id=' + t_id ,
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == 'ok') {
                                $("#container").fadeIn("slow");
                                $("#popup").fadeIn("slow");

                                $('#avgrat').text(data.average_rating);
                                $('#totalrat').text(data.rating_number);
                                setTimeout(function () {
                                    $('#popup').fadeOut();
                                }, 300000);

                            } else {
                                alert('Some problem occured, please try again.');
                            }
                        }
                    });
                    $("#div").html('<h3>Thanks for your feedback</h3>');

                }
            </script>
            <!--- popup--->

            <script type='text/javascript'>


                $(function () {

                    var feedbackLabel = "<?php echo $_REQUEST['label']; ?>";
                    var overlay = $('<div id="overlay"></div>');
                    $('li').click(function () {
                        $('#event-section').hide();
                        overlay.appendTo(document.body).remove();
                        return false;
                    });
                    $('.x').click(function () {
                        $('.popup').hide();
                        overlay.appendTo(document.body).remove();
                        return false;
                    });
                    $('.close').click(function () {
                        $('#event-section').hide();

                    });

                    if (feedbackLabel == 'feedback') {
                         $(".loader").show();
                         $("#main").hide();


                   window.onload = function () {

                            $(".loader").hide();
                             $("#main").show();

                            $('#event-section').show();
                            return false;
                        }
                    }
                });
            </script>
           <script>

           $(document).ready(function() {
                $("#close").click(function(){
			$("#event-section").hide();
		});
            </script>


            <style>
                /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
                @import url("http://fonts.googleapis.com/css?family=Open+Sans:400,600,700");
                @import url("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css");

                *, *:before, *:after {
                    margin: 0;
                    padding: 0;
                    box-sizing: border-box;
                }

                html, body {
                    height: 100%;
                }

                body {
                    font: 14px/1 'Open Sans', sans-serif;
                    color: #555;
                    background: #eee;
                }

                h1 {
                    padding: 50px 0;
                    font-weight: 400;
                    text-align: center;
                }

                p {
                    margin: 0 0 20px;
                    line-height: 1.5;
                }

                section {
                    display: none;
                    padding: 20px 0 0;
                    border-top: 1px solid #ddd;
                }

                input {
                    display: none;
                }

                label {
                    display: inline-block;
                    margin: 0 0 -1px;
                    padding: 15px 25px;
                    font-weight: 600;
                    text-align: center;
                    color: #bbb;
                    border: 1px solid transparent;
                }

                label:before {
                    font-family: fontawesome;
                    font-weight: normal;
                    margin-right: 10px;
                }

                label:hover {
                    color: #888;
                    cursor: pointer;
                }

                input:checked + label {
                    color: #555;
                    border: 1px solid #ddd;
                    border-top: 2px solid orange;
                    border-bottom: 1px solid #fff;
                }

                #tab1:checked ~ #content1,
                #tab2:checked ~ #content2,
                #tab3:checked ~ #content3,
                #tab4:checked ~ #content4 {
                    display: block;
                }

                table tr th {
                    padding: 10px 40px;
                }

                table tr td {
                    text-align: center;
                }

                @media screen and (max-width: 650px) {
                    label {
                        font-size: 0;
                    }

                    label:before {
                        margin: 0;
                        font-size: 18px;
                    }
                }

                @media screen and (max-width: 400px) {
                    label {
                        padding: 15px;
                    }
                }

                .afterg .span-6 {
                    width: 25%;
                }

                .afterp .personal-info {
                    padding: 17px;
                    color: rgba(33, 21, 32, 0.73);
                }

                .after-profile-pic .profile-pic {
                    width: 80%;

                }

                .afterp .personal-info label {
                    color: #EEF3EE;
                }

                .for-background .span12 {
                    background-color: #3D6B86;
                }

            </style>


            <main id="main">

                <input id="tab1" type="radio" name="tabs" <?php if ($_GET['l'] == 'checked') echo checked; ?> >
                <label for="tab1">Events List</label>

                <input id="tab2" type="radio" name="tabs" <?php if ($_GET['l'] == 'checked') {
                    echo 1;
                } else {
                    echo checked;
                } ?> >
                <label for="tab2">Profile</label>

                <input id="tab3" type="radio" name="tabs">
                <label for="tab3"><a href="?page_id=2112">Edit Profile</a> </label>

                <input id="tab4" type="radio" name="tabs">
                <label for="tab4"><a href="?page_id=2137">Logout</a> </label>

                <section id="content1">

                    <table class="wp-list-table widefat fixed striped pages" cellpadding="10">

                        <tbody>

                        <tr>
                            <td colspan="3"><h3> Class Event </h3></td>
                            <td colspan="5"><a href="#" class="event-button" id="expired-event"> Expired Event</a><a
                                    href="#" class="event-button" id="past-event">Past Event</a><a href="#"
                                                                                                   class="event-button"
                                                                                                   id="live-event">Live
                                    Event</a>
                                <a href="#" class="event-button" id="upcoming-event">Upcoming Event</a></td>
                        </tr>

                        <tr>
                            <th>Title</th>
                            <th>Teacher</th>
                            <th>Date & Time</th>
                            <th>End Time</th>
                            <th>Duration</th>
                            <th>Description</th>
                            <th> Action</th>
                        </tr>
                        <?php include("student-event-list.php"); ?>
                        </tbody>
                    </table>
                </section>

                <section id="content2">
                    <div class="for-background">
                        <div class="span12 afterg">
                            <div class="span-6 after-profile-pic">
                                <div class="profile-pic">
                                    <img class="thumb"
                                         src="<?php echo CHILD_URL; ?>/images/uploads/<?php if ($things[profilepic]) {
                                             echo $things[profilepic];
                                         } else {
                                             echo 'man_icon.png';
                                         } ?>">
                                </div>
                            </div>
                            <div class="span-6 afterp" style="width:69%">
                                <div class="personal-info">
                                    <div class="leftinfo">
                                        <div class="name">
                                            <div class="labels"><p>Name</p></div>
                                            <div class="colon"><p> :</p></div>
                                            <div class="text">
                                                <p><?php echo $things[First_Name] . ' ' . $things[Last_Name]; ?></p>
                                            </div>
                                        </div>
                                        <div class="gender">
                                            <div class="labels"><p>Gender</p></div>
                                            <div class="colon"><p> :</p></div>
                                            <div class="text"><p> <?php echo $things[Gender] ?></p></div>
                                        </div>
                                    </div>
                                    <div class="rightinfo">
                                        <div class="contact">
                                            <div class="labels-rightinfo"><p>Contact Number</p></div>
                                            <div class="colon"><p> :</p></div>
                                            <div class="text"><p><?php echo $things[Phone] ?></p></div>
                                        </div>
                                        <div class="address">
                                            <div class="labels-rightinfo"><p>Address</p></div>
                                            <div class="colon"><p> :</p></div>
                                            <div class="text"><p><?php echo $things[Address] ?></p></div>
                                        </div>
                                    </div>
                                    <div class="about">
                                        <div class="labels-about"><p>About</p></div>
                                        <div class="colon-about"><p> :</p></div>
                                        <div class="text-about"><p><q><?php echo $things[About]; ?></q></p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="event-button">
                        <?php
                        global $wpdb;
                        $credits = 0;
                        $students = $wpdb->get_results($wpdb->prepare("SELECT * from wp_student_credits where stu_id=%s", $things[Id]));
                        foreach ($students as $student) {
                            $credits = $student->credits_balanace;
                        }
                        ?>
                        Avalable creaits : <?php echo $credits; ?>
                    </div>
                    <br/>

                    <div style="margin-top: 40px;
    padding-left: 0px;
    margin-left: 100px;
    border-right-width: 1px;
    border-left-width: 10px;
    padding-right: 10px;
    margin-right: -10;">
                        <div class="event-button">

                            <a href="<?php echo WP_SITEURL ?>?page_id=2237">Credits History</a>
                        </div>
                    </div>
                </section>


            </main>

        </div>
    </div>
<?php
//	$resultset = mysql_query($query);
//	if(mysql_num_rows($resultset)>0)
//	{                          
//		while($row = mysql_fetch_array($resultset))
//		{							
//		?>
    <!--			<br>
			<table >
	                            <tr>
	                            <td>UserName : </td>
	                            <td> <?php echo $row["Username"]; ?></td>
	                            </tr>
	                            <tr>
	                            <td>First Name : </td>
	                            <td> <?php echo $row["First_Name"]; ?></td>
	                            </tr>
	                            <tr>
	                            <td>Last Name : </td>
	                            <td> <?php echo $row["Last_Name"]; ?></td>
	                            </tr>
	                            <tr>
	                            <td>Gender : </td>
	                            <td> <?php echo $row["Gender"]; ?></td>
	                            </tr>
	                            <tr>
	                            <td>Email : </td>
	                            <td> <?php echo $row["Email"]; ?></td>
	                            </tr>
	                            <tr>
	                            <td>Phone : </td>
	                            <td> <?php echo $row["Phone"]; ?></td>
	                            </tr>
	                            <tr>
	                            <td>About Me : </td>
	                            <td> <?php echo $row["About"]; ?></td>
	                            </tr>
	                            <tr>
	                            <td>Address : </td>
	                            <td> <?php echo $row["Address"]; ?></td>
	                            </tr>
	                            
	                            
	                            
	                            </table> -->
<?php //}
//}
?>

    <!-----------End Profile Fetching ------>

<?php
include_once 'dbConfig.php';
//Fetch rating deatails from database
$query = "SELECT rating_number, FORMAT((total_points / rating_number),1) as average_rating FROM post_rating WHERE post_id = 1 AND status = 1";
$result = $db->query($query);
$ratingRow = $result->fetch_assoc();
?>

   <!-- <div class='popup'>
    <img id="close" src="http://lms.dyalitsolutions.com/lms/wp-content/themes/lms-child/images/3.png" >
        <div class='contentss'>


            <form name="frm" method="post">

                <label class="give_feedback_text"><img width="35px" src="<?php echo CHILD_URL; ?>/images/feedback.png">&nbsp;&nbsp;Give
                    Feedback Here:-</label><br><br>

                    <div><label class="feedback_text">Feedback:</label>
                    <textarea name="feedback" value=" " class="" id="feedbackss"></textarea>
                   </div>
                <div><label class="feedback_text">Rating:</label></div>
                  <div> <input name="rating" value="0" id="rating_star" type="hidden" postID="1"></div>

                  <!--  <div class="overall-rating">(Average Rating <span
                            id="avgrat"><?php //echo $ratingRow['average_rating']; ?></span>
                        Based on <span id="totalrat"><?php //echo $ratingRow['rating_number']; ?></span> rating)</span>
                    </div> ->
                </div>

                </table>
            </form>
        </div> -->

    <div id="event-section">
        <div id="popupContact">
            <!-- Contact Us Form -->

            <div id="event-form-new">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5> Give Your FeedBack And Rating</h5>
                <hr>
                <div class="divLabel"> Feedback <br/>
                    <textarea name="feedback" value="" class="" id="feedbackss"></textarea><br><br><hr>
                   </div>
                <div class="divLabel"> Give Rating Here <br/><br/>
               <input name="rating" value="0" id="rating_star" type="hidden" postID="1"></div><br/>
                  <div id="modal-footer">  </div>
                </div>
            </div>
        </div>


    </div>

    <div id="container">
    </div>
    <div id="popup">
        <div id="div"></div>
    </div>


<?php get_footer(); ?>
    <script type="text/javascript" src="<?php echo CHILD_URL; ?>/js/rating.js"></script>
    <link href="<?php echo CHILD_URL; ?>/css/rating.css" rel="stylesheet" type="text/css">
<?php ob_flush(); ?>