jQuery(document).ready(function() {
	jQuery(window).resize(
		function(){
			if(!jQuery('body').hasClass('cherry-fixed-layout')) {
				jQuery('.full-width-block').width(jQuery(window).width());
				jQuery('.full-width-block').css({width: jQuery(window).width(), "margin-left": (jQuery(window).width()/-2), left: "50%"});
			};
		}
	).trigger('resize');

	if( !jQuery("html").hasClass("ie8") ) {
		jQuery(function(){
			jQuery('.posts-grid.our-tutors:last-of-type').find('li:last-child').prev().addClass('hidden');
		});
	}	

	if( jQuery("html").hasClass("ie8") || jQuery("html").hasClass("ie9") ) {
		jQuery(function(){
			jQuery('input, textarea').placeholder();
		});
	}
	jQuery('#cat, #city').styler({singleSelectzIndex: '999'});
})