<?php ob_start();?>
<?php /*
Template Name: Student Feedback 
*/

?>

<?php get_header();?>
<style type="text/css">
#overlay {
position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background-color: #000;
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
z-index: 100;
display: none;
}
.contentss a{
text-decoration: none;
}
.popup{
width: 50%;
margin: 0 auto;
display: none;
position: fixed;
z-index: 101;
}
.contentss{
margin: 0px 429px;
max-width: 500px;
min-width: 250px;
width: 100%;
padding: 10px 50px;
border: 2px solid #808080;
height: auto;
border-radius: 10px;
font-family: raleway;
background-color: #F5F5F5;
float: left;

box-shadow: 0 2px 5px #000;
color: #000;
font-size: 16px;
}
.contentss p{
clear: both;
color: #555555;
text-align: justify;
}
.contentss p a{
color: #d91900;
font-weight: bold;
}
.contentss .x{
float: right;
height: 35px;
left: 22px;
position: relative;
top: -25px;
width: 34px;
}
.content .x:hover{
cursor: pointer;
}
.feedback_text{
font-size: 24px;
line-height: 2.5em;
font-family: initial;
color: rgb(18, 26, 26);
}
.give_feedback_text{
font-size: 22px;
}
#feedbackss{
resize: both;
height:88px;
font-size:20px;
}
#close{
	
}
#p{

 cursor: pointer;
}
	
#container{
	height:100%;
	width:100%;
	background:#000;
	top:0;
	right:0;
	left:0;
	bottom:0;
	position: fixed;
	z-index: 1000;
	opacity: 0.9;
	display:none;
    opacity:0;
}
#popup{
	margin: 0px auto;
	height:170px;
	width:500px;
	top:160px;
	right:20px;
	left:20px;
	bottom:10px;
	position: absolute;
	z-index: 1500;
	box-shadow: 0 0 50px #000;
	display:none;
    border: 2px solid #B1A9A9;
	border-radius: 10px;
}
#label{
cursor: pointer;
color:aqua;
}
#div h3{
font: 28px/130px Roboto;
color: #2D426F;
text-align: center;
}
 </style>
 <style type="text/css">
    .overall-rating{font-size: 14px;margin-top: 5px;color: #D83636;}
 </style>

 <script>
 $(function() {
    $("#rating_star").codexworld_rating_widget({
        starLength: '5',
        initialValue: '',
        callbackFunctionName: 'processRating',
        imageDirectory: 'images/',
        inputAttr: 'postID'
    });
    });

function processRating(val, attrVal){
	var feedback = $('#feedbackss');
	var z="<?php echo CHILD_URL; ?>";
    $.ajax({
        type: 'POST',
        url: z+'/rating.php',
        data: 'postID='+attrVal+'&ratingPoints='+val+'&feedback='+feedback.val(),
        dataType: 'json',
        success : function(data) {
            if (data.status == 'ok') {
				$("#container").fadeIn("slow");
				$("#popup").fadeIn("slow");

                $('#avgrat').text(data.average_rating);
                $('#totalrat').text(data.rating_number);
				setTimeout(function() {
             $('#popup').fadeOut();
			   }, 3000);
                              
						}else{
                alert('Some problem occured, please try again.');
            }
        }
    });
	$("#div").html('<h3>Thanks for your feedback</h3>');
	
    }
</script>
<!--- popup--->

<script type='text/javascript'>
$(function(){
	
var overlay = $('<div id="overlay"></div>');
$('li').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});
$('.x').click(function(){
$('.popup').hide();
overlay.appendTo(document.body).remove();
return false;
});
	window.onload = function(){
overlay.show();
overlay.appendTo(document.body);
$('.popup').show();
return false;
}
});
</script>
<!--- popup end here --->


<?php
		include_once 'dbConfig.php';
		//Fetch rating deatails from database
		$query = "SELECT rating_number, FORMAT((total_points / rating_number),1) as average_rating FROM post_rating WHERE post_id = 1 AND status = 1";
		$result = $db->query($query);
		$ratingRow = $result->fetch_assoc();
		?>
		<div class='popup' >
		<div class='contentss'>
		
		<form name="frm" method="post" >
	 
	    <label class="give_feedback_text"><img width="35px" src="<?php echo CHILD_URL; ?>/images/feedback.png">&nbsp;&nbsp;Give Feedback Here:-</label><br><br>
	    <div><label class="feedback_text">Feedback:</label>
	    <textarea name="feedback" value=" "  class="" id="feedbackss"></textarea>
	    </div>
		<div><label class="feedback_text">Rating:</label>
		<input name="rating" value="0" id="rating_star" type="hidden" postID="1" >
		<div class="overall-rating">(Average Rating <span id="avgrat"><?php echo $ratingRow['average_rating']; ?></span>
		Based on <span id="totalrat"><?php echo $ratingRow['rating_number']; ?></span>  rating)</span></div>
	    </div>
		
		</table>
	    </form>
		</div>
		</div>
		<button class="click1">click</button>
		<div id="container">
		</div>
		<div id="popup">
		<div id="div"></div>
			</div>
		
		<?php get_footer();?>
		<script type="text/javascript" src="<?php echo CHILD_URL; ?>/js/rating.js"></script>
		<link href="<?php echo CHILD_URL; ?>/css/rating.css" rel="stylesheet" type="text/css">




