
<?php @ob_start(); session_start(); ?>
<?php
/*
Template Name: Logout
*/
?>
<?php
get_header();
?>



<?php
unset($_SESSION['username']);
if(session_destroy()) // Destroying All Sessions
{
header("location:?page_id=2020"); // Redirecting To Home Page
}
else
{
echo "Error";
}
?>



<?php get_footer();?>
<?php ob_flush();?>