<?php
	// Loads child theme textdomain
	load_child_theme_textdomain( CURRENT_THEME, CHILD_DIR . '/languages' );

	add_filter( 'cherry_stickmenu_selector', 'cherry_change_selector' );
	function cherry_change_selector($selector) {
		$selector = 'header .nav-wrap';
		return $selector;
	}

	function my_mce4_options( $init ) {
	$default_colours = '
	    "000000", "Black",        "993300", "Burnt orange", "333300", "Dark olive",   "003300", "Dark green",   "003366", "Dark azure",   "000080", "Navy Blue",      "333399", "Indigo",       "333333", "Very dark gray", 
	    "800000", "Maroon",       "FF6600", "Orange",       "808000", "Olive",        "008000", "Green",        "008080", "Teal",         "0000FF", "Blue",           "666699", "Grayish blue", "808080", "Gray", 
	    "FF0000", "Red",          "FF9900", "Amber",        "99CC00", "Yellow green", "339966", "Sea green",    "33CCCC", "Turquoise",    "3366FF", "Royal blue",     "800080", "Purple",       "999999", "Medium gray", 
	    "FF00FF", "Magenta",      "FFCC00", "Gold",         "FFFF00", "Yellow",       "00FF00", "Lime",         "00FFFF", "Aqua",         "00CCFF", "Sky blue",       "993366", "Brown",        "C0C0C0", "Silver", 
	    "FF99CC", "Pink",         "FFCC99", "Peach",        "FFFF99", "Light yellow", "CCFFCC", "Pale green",   "CCFFFF", "Pale cyan",    "99CCFF", "Light sky blue", "CC99FF", "Plum",         "FFFFFF", "White"
	';
	$custom_colours = '
	    "2d426f", "Dark Blue Template", "49bffc", "Blue Template", "82b933", "Green Template",
	';
	$init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']'; // build colour grid default+custom colors
	$init['textcolor_rows'] = 6; // enable 6th row for custom colours in grid
	return $init;
	}
	add_filter('tiny_mce_before_init', 'my_mce4_options');
	
	function template_chooser($template)   
	{    
	  global $wp_query;   
	  //$post_type = get_query_var('teacher_search'); 
	  $search_refer = $_GET["teacher_search"];
	  if($search_refer === "true") 
	  {
	    return locate_template('search-portfolio.php');  
	  }   
	  return $template;   
	}
	add_filter('template_include', 'template_chooser');  

	// Button
	if (!function_exists('button_shortcode')) {
		function button_shortcode( $atts, $content = null, $shortcodename = '' ) {
			extract(shortcode_atts(
				array(
					'link'    => 'http://www.google.com',
					'text'    => __('Read more', CHERRY_PLUGIN_DOMAIN),
					'size'    => 'normal',
					'style'   => '',
					'target'  => '_self',
					'display' => '',
					'class'   => '',
					'icon'    => 'no'
			), $atts));

			$output =  '<a href="'.$link.'" title="'.$text.'" class="btn btn-'.$style.' btn-'.$size.' btn-'.$display.' '.$class.'" target="'.$target.'">';
			$output .= $text;
			if ($icon != 'no') {
				$output .= '<i class="icon-'.$icon.'"></i>';
			}
			$output .= '</a><!-- .btn -->';

			$output = apply_filters( 'cherry_plugin_shortcode_output', $output, $atts, $shortcodename );

			return $output;
		}
		add_shortcode('button', 'button_shortcode');
	}

	// Loads custom scripts.
	require_once( 'custom-js.php' );
	require_once( 'shortcodes/service-box.php' );
	require_once( 'shortcodes/posts-grid.php' );
	require_once( 'shortcodes/search-box.php' );

add_action( 'init', 'codex_book_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_book_init() {
	$labels = array(
		'name'               => _x( 'Packages', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Package', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Packages', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Package', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'book', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Package', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Package', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Package', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Package', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Packages', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Packages', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Packages:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Packages found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Packages found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'book' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title')
	);

	register_post_type( 'package', $args );
}



?>