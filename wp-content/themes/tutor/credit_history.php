<?php get_header(); ?>
<?php session_start();  ?>
  <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url("http://fonts.googleapis.com/css?family=Open+Sans:400,600,700");
@import url("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css");
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

html, body {
  height: 100%;
}

body {
  font: 14px/1 'Open Sans', sans-serif;
  color: #555;
  background: #eee;
}

h1 {
  padding: 50px 0;
  font-weight: 400;
  text-align: center;
}

p {
  margin: 0 0 20px;
  line-height: 1.5;
}

section {
  display: none;
  padding: 20px 0 0;
  border-top: 1px solid #ddd;
}

input {
  display: none;
}

label {
  display: inline-block;
  margin: 0 0 -1px;
  padding: 15px 25px;
  font-weight: 600;
  text-align: center;
  color: #bbb;
  border: 1px solid transparent;
}

label:before {
  font-family: fontawesome;
  font-weight: normal;
  margin-right: 10px;
}

label:hover {
  color: #888;
  cursor: pointer;
}

input:checked + label {
  color: #555;
  border: 1px solid #ddd;
  border-top: 2px solid orange;
  border-bottom: 1px solid #fff;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4 {
  display: block;
}
table tr th
{
 padding:10px 40px;
}
table tr td
{
 text-align:center;
}
@media screen and (max-width: 650px) {
  label {
    font-size: 0;
  }

  label:before {
    margin: 0;
    font-size: 18px;
  }
}
@media screen and (max-width: 400px) {
  label {
    padding: 15px;
  }
}

    </style>



<?php
/*
Template Name: credit history
*/
$username=$_SESSION['username'];
global $wpdb;
$query = "SELECT * FROM register_student where username = '$username'";
  $results = $wpdb->get_results($query);
  $things = array();
  //print_r($results[0]);
  foreach( $results[0] as $key => $result ) {
  $things[$key] = $result;
  }



?>

<table class="wp-list-table widefat fixed striped pages" cellpadding="10">
      <tbody>
 <tr ><td colspan="5"> <h3> Credit History </h3></td> 
     </tr>
             
             <tr><th>Username</th><th>Title</th><th>Teacher</th><th>Description</th><th>Date</th><th>Time</th><th>Duration</th><th>Credits</th><th> Action</th></tr>
 <?php 
global $wpdb;
$q="select register_student.Username,wp_classes.class_title,wp_classes.teacher_name,wp_classes.description,wp_classes.start_date,wp_classes.time,wp_classes.duration,wp_credits_uses.used_credits from wp_credits_uses join wp_classes on wp_credits_uses.event_id=wp_classes.class_id join register_student on wp_credits_uses.student_id=register_student.Id where wp_credits_uses.student_id= '$things[Id]'";
 $history_data = $wpdb->get_results($q);            
foreach($history_data as $data){

?>
<tr class="upcoming-event"><td><?php echo $data->Username; ?></td><td><?php echo $data->class_title; ?></td><td> <?php echo $data->teacher_name; ?></td>  <td><?php echo $data->description; ?></td><td><?php echo $data->start_date; ?></td><td><?php echo $data->time; ?></td><td><?php echo $data->duration; ?></td><td><?php echo $data->used_credits; ?></td><td>
<a href="#" title="Edit Event" style="margin-right: 5px;"><img style="width: 20px;" src="<?php bloginfo('template_url'); ?>/images/icon-edit.png"></a><a style="margin-right: 5px;" href="#" title="Enroll"><img style="width: 20px;" src="<?php bloginfo('template_url'); ?>/images/icon-add.png"></a>
<a href="#" title="Delete Event" style="margin-right: 5px;"><img style="width: 20px;" src="<?php bloginfo('template_url'); ?>/images/icon-delete.png"></a></td>

     </tr>
<?php } ?>
</tbody>
</table>
<?php get_footer(); ?>







