<script src="<?php echo CHILD_URL; ?>js/prefixfree.min.js"></script>
<?php ob_start(); ?>
<?php get_header(); ?>

<link rel='stylesheet' href='<?php echo CHILD_URL; ?>/css/jquery-ui.min.css'/>
<link href='<?php echo CHILD_URL; ?>/css/fullcalendar.css' rel='stylesheet'/>
<script src='<?php echo CHILD_URL; ?>/js/moment.min.js'></script>
<script src='<?php echo CHILD_URL; ?>js/jquery.min.js'></script>
<script>
    $(document).ready(function () {
        $(".close").click(function () {

            $("div[id^='event-section-']").hide();
        });
        $(document).on("click", ".edit-popup", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-href");

            $("#" + id).show();
        });
    });
</script>
<script>

    $(document).ready(function () {
        $(".launch").click(function () {

            /*  var status="
            <?php  echo $_SESSION['CLASS_STATUS']; ?>";
             if(status=='EXPIRED'){
             alert("Class Expired");
             }
             if(status=='WAITING'){
             alert("Their is still Time  Left in Class Start");
             }
             var sess_code = $(this).attr('data-code');
             var student_id= $(this).attr('id');
             $.ajax({
             type: "POST",
             url: "http://privatetutor.dyalitsolutions.com/launch.php",
             data: { student_id : student_id , sess_code : sess_code },
             dataType:"json",
             beforeSend: function(){console.log('Sending.......');},
             success : function(data){ alert(data.status); }



             });

             */


        });
    });
</script>
<script>
    function confirmDelete(class_id, session_code) {
        var classid = class_id;
        var sess_code = session_code;


        if (confirm("Are u sure to delete the Class Event")) {

            $.ajax({
                type: "POST",
                url: "<?php echo WP_SITEURL?>/CancelClass.php",
                data: {class_id: classid, sess_code: sess_code},
                dataType: "json",
                beforeSend: function () {
                    console.log('Sending.......');
                    $(".upcoming-event").hide();
                    $("#loader").show();
                },
                success: function (data) {

                    $('body').find('#row_'+classid).remove();
                    $(".upcoming-event").show();
                    $("#loader").hide();

                },

                error: function() {
                   alert("unable to cancel the class");
                    $(".upcoming-event").show();
                    $("#loader").hide();
                }

            });


        }

    }

</script>


<?php
global $wpdb;
 //print_r($_GET['feedback']);die();
$teacher_event = $wpdb->get_results($wpdb->prepare("SELECT * from class_students as a ,wp_classes as b  where a.student_id=%s AND a.class_session_code = b.class_session_code ", $things[Id]));
 //echo '<pre>'; print_r($teacher_event);die();

foreach ($teacher_event as $event) {

    if ($event->session_status == '1') {

        ?>
        <tr  >
            <td colspan="10" id="loader"  style="display:none;" >
            </td>
        </tr>
        <tr class="upcoming-event" id="row_<?php echo $event->class_id; ?>">
            <td><?php echo $event->class_title; ?></td>
            <td><?php echo $event->teacher_name; ?></td>
            <td><?php $e_date = strtotime($event->start_date);
                echo date("Y-M-d", $e_date); ?><?php echo date("h:i A", $event->startingtimestamp); ?></td>
            <td><?php echo date("h:i A", $event->endingtimestamp); ?> </td>
            <td><?php echo $event->duration; ?></td>
            <td><?php echo $event->description; ?></td>
            <td>
                <a class="edit-popup" data-href="event-section-<?php echo $event->class_id; ?>"
                   href="#popupContact-<?php echo $event->class_id; ?>" title="Edit Event"
                   style="margin-right: 5px;"><img style="width: 20px;"
                                                   src="<?php bloginfo('template_url'); ?>/images/icon-edit.png"></a><a
                    href="#" class_id="<?php echo $event->class_id; ?>"
                    onclick="return confirmDelete('<?php echo $event->class_id; ?>','<?php echo $event->class_session_code; ?>')"
                    title="Delete Event" style="margin-right: 5px;"><img style="width: 20px;"
                                                                         src="<?php bloginfo('template_url'); ?>/images/icon-delete.png"></a>

                <a class="launch" href="#" id="<?php echo $event->student_id; ?>"
                   data-code="<?php echo $event->class_session_code; ?>" title="Launch Event"
                   style="margin-right: 5px;"><img style="width: 20px;"
                                                   src="http://privatetutor.dyalitsolutions.com/wp-content/uploads/2016/01/launch.png"></a>


                <div id="event-section-<?php echo $event->class_id; ?>">
                    <!-- Popup Div Starts Here -->
                    <div id="popupContact-<?php echo $event->class_id; ?>">
                        <!-- Contact Us Form -->
                        <form method="post"
                              action="http://privatetutor.dyalitsolutions.com/?page_id=2254&id=<?php echo $event->class_id; ?>&class_s_code=<?php echo $event->class_session_code; ?>">
                            <div id="event-form-<?php echo $event->class_id; ?>">
                                <img id="close" class="close"
                                     src="http://lms.dyalitsolutions.com/lms/wp-content/themes/lms-child/images/3.png">

                                <h2 class="form-title">Edit Event</h2>
                                <hr>
                                <input id="name" class="input-field" name="class_title" placeholder="Title"
                                       value="<?php echo $event->class_title; ?>" type="text" required>
                                <input id="m_id" name="manager_id" value="<?php echo $event->manager_id; ?>"
                                       type="hidden">
                                <input name="teacher_id" value="<?php echo $event->teacher_id; ?>" type="hidden">
                                <input name="teacher_name" value="<?php echo $event->teacher_name; ?>" type="hidden">
                                <input type="hidden" name="email_to" value="<?php echo $things[Email]; ?>">
                                <input type="hidden" name="email_from" value="<?php echo $event->teacher_email; ?>">
                                <input id="event-time" class="input-field"
                                       value="<?php $date = date("h:i", strtotime($event->time));
                                       echo $date; ?>" name="time" placeholder="Time (e.g 9:00 AM)" type="time"
                                       required>
                                <input id="event-date" class="input-field" value="<?php echo $event->start_date; ?>"
                                       name="end_date" value="" placeholder="End Date" type="text" required>

                                <select name="duration" id="event-duration" class="input-field"
                                        value="<?php echo $event->duration; ?>">
                                    <option
                                        value="<?php echo $event->duration; ?>"><?php echo $event->duration; ?></option>

                                    <option value="30">30</option>
                                    <option value="60">60</option>
                                    <option value="90">90</option>
                                    <option value="120">120</option>
                                    <option value="150">150</option>
                                </select>
                                <textarea id="msg" class="input-field" name="desc"
                                          placeholder="Description"><?php echo $event->description; ?></textarea>
                                <input type="submit" id="submit" onClick="" value="Save">
                        </form>
                    </div>
                </div>
                <!-- Popup Div Ends Here -->
                </div>
            </td>
        </tr>
        <?php
    }

    if ($event->session_status == '2') {
        ?>
        <tr class="live-event" style="display:none">
            <td><?php echo $event->class_title; ?></td>
            <td><?php echo $event->teacher_name; ?></td>
            <td><?php $e_date = strtotime($event->start_date);
                echo date("Y-M-d", $e_date); ?><?php echo date("h:i A", $event->startingtimestamp); ?></td>
            <td><?php echo date("h:i A", $event->endingtimestamp); ?> </td>
            <td><?php echo $event->duration; ?></td>
            <td><?php echo $event->description; ?></td>
            <td><a class="launch" id="<?php echo $event->student_id; ?>"
                   href="<?php echo WP_SITEURL; ?>?page_id=2246&class_session_code=<?php echo base64_encode($event->class_session_code); ?>&student_id=<?php echo base64_encode($event->student_id); ?>"
                   data-code="<?php echo $event->class_session_code; ?>" title="Launch Event"
                   style="margin-right: 5px;"><img style="width:40px;"
                                                   src="<?php echo WP_SITEURL; ?>wp-content/uploads/2016/01/launch.png"></a><a
                    href=""> </a></td>
        </tr>
        <?php
    }


    if ($event->session_status == '3') {
        ?>
        <tr class="past-event" style="display:none">
            <td><?php echo $event->class_title; ?></td>
            <td><?php echo $event->teacher_name; ?></td>
            <td><?php $e_date = strtotime($event->start_date);
                echo date("Y-M-d", $e_date); ?><?php echo date("h:i A", $event->startingtimestamp); ?></td>
            <td><?php echo date("h:i A", $event->endingtimestamp); ?></td>
            <td><?php echo $event->duration; ?></td>
            <td><?php echo $event->description; ?></td>
            <td>
                <a href="<?php echo WP_SITEURL; ?>?page_id=2287&class_session_code=<?php echo $event->class_session_code; ?>&student_id=<?php echo $event->student_id; ?>"
                   title="Recording" style="margin-right: 5px;"><img style="width: 20px;"
                                                                     src="<?php echo WP_SITEURL; ?>wp-content/uploads/2016/01/video_run.svg"></a><a
                    href="#" title="Delete Event" style="margin-right: 5px;"><img style="width: 20px;"
                                                                                  src="http://lms.dyalitsolutions.com/lms/wp-content/themes/lms-child/images/icon-delete.png"></a>
            </td>
        </tr>
        <?php
    }
    if ($event->session_status == '4') {
        ?>
        <tr class="expired-event" style="display:none">
            <td><?php echo $event->class_title; ?></td>
            <td><?php echo $event->teacher_name; ?></td>
            <td><?php $e_date = strtotime($event->start_date);
                echo date("Y-M-d", $e_date); ?><?php echo date("h:i A", $event->startingtimestamp); ?></td>
            <td><?php echo date("h:i A", $event->endingtimestamp); ?></td>
            <td><?php echo $event->duration; ?></td>
            <td><?php echo $event->description; ?></td>
            <td><a href="#" class="delete" id="<php echo $event->class_session_code; ?>" title="Delete Event"
                   style="margin-right: 5px;"><img style="width: 20px;"
                                                   src="http://lms.dyalitsolutions.com/lms/wp-content/themes/lms-child/images/icon-delete.png"></a>
            </td>
        </tr>


        <?php
    }


}
?>


<!--

http://privatetutor.dyalitsolutions.com/?page_id=2246&class_session_code=<?php echo $event->class_session_code; ?>&student_id=<?php echo $event->student_id; ?>
-->




