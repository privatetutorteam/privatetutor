<?php
session_start();
//Database configuration
$dbHost = 'localhost';
$dbUsername = 'root';
$dbPassword = 'sp';
$dbName = 'privatetutor';

//Connect with the database
$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);
if($db->connect_errno):
    die('Connect error:'.$db->connect_error);
endif;
if(!empty($_POST['ratingPoints'])){
    $postID = $_POST['postID'];
    $ratingNum = 1;
    $ratingPoints = $_POST['ratingPoints'];
	$student_id=$_POST['stu_id'];
    $tutor_id=$_POST['t_id'];
	$fdback=$_REQUEST['feedback'];

    //Check the rating row with same post ID
    $prevRatingQuery = "SELECT * FROM post_rating WHERE tutor_id = '".$tutor_id."' and student_id=".$student_id;
    $prevRatingResult = $db->query($prevRatingQuery);
    if($prevRatingResult->num_rows > 0):
        $prevRatingRow = $prevRatingResult->fetch_assoc();
        $ratingNum = $prevRatingRow['rating_number'] + $ratingNum;
        $ratingPoints = $prevRatingRow['total_points'] + $ratingPoints;
        //Update rating data into the database
        $query = "UPDATE post_rating SET rating_number = '".$ratingNum."', total_points = '".$ratingPoints."', modified = '".date("Y-m-d H:i:s")."' WHERE post_id = ".$postID;
        $update = $db->query($query);
    else:
        //Insert rating data into the database
        $query = "INSERT INTO post_rating (student_id,feedback,post_id,tutor_id,rating_number,total_points,created,modified)
		VALUES('".$student_id."','".$fdback."','".$postID."','".$tutor_id."','".$ratingNum."','".$ratingPoints."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')";
        $insert = $db->query($query);
    endif;
    
    //Fetch rating deatails from database
    $query2 = "SELECT rating_number, FORMAT((total_points / rating_number),1) as average_rating FROM post_rating WHERE tutor_id = ".$tutor_id." AND status = 1";
    $result = $db->query($query2);
    $ratingRow = $result->fetch_assoc();
    
    if($ratingRow){
        $ratingRow['status'] = 'ok';
    }else{
        $ratingRow['status'] = 'err';
    }
    
    //Return json formatted rating data
    echo json_encode($ratingRow);
}
?>

