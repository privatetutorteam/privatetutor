<?php
/*
Template Name: class-detail 

*/
get_header();
?>
<style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url("http://fonts.googleapis.com/css?family=Open+Sans:400,600,700");
@import url("http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css");
*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

html, body {
  height: 100%;
}

body {
  font: 14px/1 'Open Sans', sans-serif;
  color: #555;
  background: #eee;
}

h1 {
  padding: 50px 0;
  font-weight: 400;
  text-align: center;
}

p {
  margin: 0 0 20px;
  line-height: 1.5;
}

section {
  display: none;
  padding: 20px 0 0;
  border-top: 1px solid #ddd;
}

input {
  display: none;
}

label {
  display: inline-block;
  margin: 0 0 -1px;
  padding: 15px 25px;
  font-weight: 600;
  text-align: center;
  color: #bbb;
  border: 1px solid transparent;
}

label:before {
  font-family: fontawesome;
  font-weight: normal;
  margin-right: 10px;
}

label:hover {
  color: #888;
  cursor: pointer;
}

input:checked + label {
  color: #555;
  border: 1px solid #ddd;
  border-top: 2px solid orange;
  border-bottom: 1px solid #fff;
}

#tab1:checked ~ #content1,
#tab2:checked ~ #content2,
#tab3:checked ~ #content3,
#tab4:checked ~ #content4 {
  display: block;
}
table tr th
{
 padding:10px 40px;
}
table tr td
{
 text-align:center;
}
@media screen and (max-width: 650px) {
  label {
    font-size: 0;
  }

  label:before {
    margin: 0;
    font-size: 18px;
  }
}
@media screen and (max-width: 400px) {
  label {
    padding: 15px;
  }
}
.afterg .span-6{
	width:25%;
}
.afterp .personal-info{
	padding:17px;
	color: rgba(33, 21, 32, 0.73);
}
.after-profile-pic .profile-pic{
	width:100%;
	
}
.afterp .personal-info label{
	color: #EEF3EE;
}
.for-background .span12{
background-color: #3D6B86;
}

    </style>


<table class="wp-list-table widefat fixed striped pages" cellpadding="10">
      <tbody>
            <thead>
	            <tr >
	            	<td colspan="3"> <h3> Class Detail </h3></td>
	            </tr>
	             <tr>
	             	<th>Title</th>
	             	<th>Teacher Name</th>
	             	<th>Date & Time</th>
	             	<th>Duration</th>
	             	<th>Description</th>
	             </tr>
	     </thead>
	     
   	     <?php
	             global $wpdb;
	             $sess_code=$_GET['session_code'];
	             $manager_id=$_GET['manager_id'];
	             $sql = "select * from wp_classes where class_session_code='$sess_code' AND manager_id='$manager_id'";
	             $result=$wpdb->get_results($sql);
	             foreach($result as $class){
	      		//print_r($class);
	      ?>
	             <tr>
	             	<td> <?php echo $class->class_title; ?> </td>
	             	<td> <?php echo $class->teacher_name; ?> </td>
	             	<td> <?php echo $class->start_date." ".$class->time; ?></td>
	             	<td> <?php echo $class->duration." Min"; ?></td>
	             	<td> <?php echo $class->description; ?> </td>
	             </tr>
                 <?php } ?>
       </tbody>
 </table>

