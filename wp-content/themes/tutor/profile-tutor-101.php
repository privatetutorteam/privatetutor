<?php
/*
Template Name:profile-tutor-101;
*/
get_header();
?>

<style>
    .profile-title h4{
        font: 20px/36px Roboto;
        color: blue;
    }
    .profile-img
    {
        width:30%;
        float:left;
    }
    #teacher-content
    {
        width:60%;
        padding-top:0px !important;
        margin-left: 17px ;
    }
    .profile-rating
    {
        width:57%;
    }
    .view-profile
    {
        width: 33%;
        float: left;
        margin-left: 5%
    }
    .hours-complete
    {
        float:left;
    }
    .hours-complete h5{
        font: 16px/15px Roboto !important;
        color: #49BFFC;
    }
    .hourly-rate
    {
        float:left;
        margin-left:12px;
    }
    .hourly-rate h6
    {
        font: 16px/15px Roboto !important;
        color: #49BFFC;
    }
    .profile-subject h6
    {
        font: 16px/17px Roboto !important;
    }
    .span4
    {
        background-color: rgb(237, 245, 245);
    }
    .span4:hover
    {
        background-color: #EDEDC2 !important;
    }
    .view-profile a button{
        padding: 5px 11px !important;
    }
</style>



<div class="row" style="margin-top: 20px;padding-bottom: 20px;border-bottom: 1px solid rgba(204, 204, 204, 0.35);">

    <?php
   $rating='';
    global $wpdb;
    if(isset($_POST['submit'])){
        $post_id = $wpdb->get_results("SELECT * FROM register where SKILLS LIKE '%".$_POST['cat']."%'");

    }else{
        $post_id = $wpdb->get_results("SELECT * FROM register");

    }

    //$post_id = $wpdb->get_results("SELECT * FROM register");
    $count = '1';
    foreach($post_id as $key)
    {
        $id=$key->Id;
        $query = "SELECT SUM(rating_number)as rating_number,SUM(FORMAT((total_points / rating_number),1))/COUNT(rating_id) as average_rating FROM post_rating WHERE tutor_id ='$id'  AND status = 1";
        $res=$wpdb->get_results($query);
        $rating=$res[0]->average_rating;
        
    if($count % 3 == 0)
    {
    ?>
        <script>
            $(function() {
                var rate='<?php echo $rating; ?>';
                $("#rating_star").codexworld_rating_widget({
                    starLength: '5',
                    initialValue: +rate,

                    imageDirectory: 'images/',
                    inputAttr: 'postID'
                });
            });


        </script>

    <div class="span4">
        <div class="profile-img"><a href="?page_id=2108&tutor_id=<?php echo $key->Id; ?>"><img style="height:145px;width:100%;" src="http://privatetutor.dyalitsolutions.com/wp-content/themes/tutor/images/uploads/<?php echo $key->profilepic; ?>"></a></div>
        <div id="teacher-content" style="float:left;"><div class="profile-title"><h4 style="margin: 0px;"><?php echo $key->First_Name . " " . $key->Last_Name; ?></h4></div>
            <div class="profile-rating"><input name="rating" value="0" id="rating_star" type="hidden" postID="1" ></div>
            <div class="profile-subject"><h6 style="margin-top: 0px;"><?php echo substr($key->Skills ,0,15);  ?></h6></div>
            <div class="hours-complete"><h5 style="margin-top: 0px;">20hrs done</h5></div>
            <div class="hourly-rate"><h6 style="margin: 0px;">$ <?php echo $key->hourlyrate; ?> / h</h6></div>

        </div>
        <div class="view-profile"><a href="?page_id=2108&tutor_id=<?php echo $key->Id; ?>"><button class="view">View Profile</button></a></div>
    </div>
</div>

<div class="row" style="margin-top: 20px;border-bottom: 1px solid rgba(204, 204, 204, 0.35);">
    <?php
    }
    else
    {
        ?>

        <div class="span4">
            <div class="profile-img"><a href="?page_id=2108&tutor_id=<?php echo $key->Id; ?>"><img style="height: 145px;width: 100%;" src="http://privatetutor.dyalitsolutions.com/wp-content/themes/tutor/images/uploads/<?php echo $key->profilepic; ?>"></a></div>
            <div id="teacher-content" style="float:left;"><div class="profile-title"><h4 style="margin: 0px;"><?php echo $key->First_Name . " " . $key->Last_Name; ?></h4></div>
                <div class="profile-rating"><img class="rating-starts" src="http://privatetutor.dyalitsolutions.com/wp-content/uploads/2015/12/stars-small.png"></div>
                <div class="profile-subject"><h6 style="margin-top: 0px;"><?php echo substr($key->Skills ,0,15);  ?></h6></div>
                <div class="hours-complete"><h5 style="margin-top: 0px;">20hrs done</h5></div>
                <div class="hourly-rate"><h6 style="margin: 0px;">$20 <?php echo $key->hourlyrate; ?> / h</h6></div>

            </div>
            <div class="view-profile"><a href="?page_id=2108&tutor_id=<?php echo $key->Id; ?>"><button class="view">View Profile</button></a></div>
        </div>

        <?php
    }
    $count++;
    }
    ?>

</div>
<style type="text/css">
    .view-profile a button{background-color: #1B294A;color: #ffffff;padding: 5px 15px;border-radius: 10px;}
</style>
<?php  get_footer(); ?>