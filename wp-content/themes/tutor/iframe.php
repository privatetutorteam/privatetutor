<?php
/**
* Template Name: Full Screen No HF
*/

get_header('iframe'); ?>

<?php the_post(); the_content(); ?>

<?php get_footer('iframe'); ?>