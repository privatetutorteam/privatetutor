<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'privatetutor');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'sp');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://localhost/privatetutor/');
define('WP_SITEURL','http://localhost/privatetutor/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z0`tcI1WQke,-!@p^8r6S{VWZWcC<3c~(+Zt&}5|TBe|G00g9LZ~WK9eieMp_0?8');
define('SECURE_AUTH_KEY',  '{?UQE%}*vF_f:-X#c/B=~{gG}CrI{|s:lDg$ulh:HhKgUg7h#hdo8oW8/f(GXNl>');
define('LOGGED_IN_KEY',    '6gjK7GC$<.}vs;=B:?7F889aJX:!03?u2%@p1+r83kE<F9BVD5,@xv4lny<:<*.-');
define('NONCE_KEY',        ':EN!e&Esg.qX4H8E:q>DqPQPl10+.W+D0:/q+2fhf|tc0ygsR|{rz4je[?a%G#wM');
define('AUTH_SALT',        'K3Oo>ye2o488oy%R>Y!.Y.k/A|<k>Wqo:+[A?ZIx9mfUe#1!WHVN-4>Mu4d9{ql&');
define('SECURE_AUTH_SALT', 'D:p}S`n_@7~^mZ*g{p-z=H!yENhD@[_?zqygZa-iBSW-Q:=S5X<5w<H5cXd5!6<e');
define('LOGGED_IN_SALT',   's>5zCF|VZbd@Y<}qC[P@3xRW^sCTszapkUIctbqOClrii_!)UZSrIp2u[wG8[}=J');
define('NONCE_SALT',       'z 1NPJGia)}O`XDF6|dG:72{@2OzW.#nttr;7&-(BQ|Zw|cZlB:M9V(ZK0Y9f=N+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
