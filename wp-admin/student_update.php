<?php
$id=$_GET['id'];
global $wpdb;

global $wpdb;
$fname =  $_REQUEST['fname'];
$lname =  $_REQUEST['lname'];
$gender = $_REQUEST['gender'];
$about =  $_REQUEST['about'];
$phone =  $_REQUEST['phone'];
$address =$_REQUEST['address'];

$profile=$_FILES["imgfile"]["name"];

if(!empty($profile)){

$target_dir = WP_SITEURL."/wp-content/themes/tutor/images/uploads";
$target_file = $target_dir . '/' . $_FILES["imgfile"]["name"];
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["imgfile"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["imgfile"]["size"] > 700000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["imgfile"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["imgfile"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}


$result = $wpdb->update( 
	'register_student', 
	array( 
		'First_Name' => $fname,	// string
		'Last_Name' => $lname,
                 'Gender'=> $gender,	// integer (number)
                 'About'=> $about,
                 'Phone'=> $phone,
                 'Address'=> $address,
                 'profilepic'=>$profile
            ),
        
	array( 'Id' => $id ), 
	array( 
		'%s',	// value1
		'%s',	// value2
                '%s',
                '%s',
                '%s',
                '%s',
                '%s'
             ),

 
	array( '%s' ) 
	
);


if($result)
{
	header('location: http://privatetutor.dyalitsolutions.com/wp-admin/admin.php?page=private_tutor_students_list');
}


else
{

}

}


?>